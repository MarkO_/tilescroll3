all: ./build/tileScroll.dsk

./build/t.bin: tileScroll.asm	./includes/tileScroll_inc.asm ./includes/diskRoutines.asm ./includes/blockManager.asm ./includes/display.asm ./includes/compiledSprites.asm ./includes/structs.asm ./includes/tunes.asm
	lwasm --6309 --decb --format=decb --list=./build/tileScroll.lst --symbol-dump=./build/tileScroll.s --symbols --map=./build/tileScoll.map --output=./build/t.bin ./tileScroll.asm

./build/sound.bin: sound.asm	./sound.asm
	lwasm --6309 --decb --format=decb --list=./build/sound.lst --symbol-dump=./build/sound.s --symbols --map=./build/sound.map --output=./build/sound.bin ./sound.asm

./build/ldTiles.bin: loadTiles.asm	./includes/tileScroll_inc.asm ./includes/diskRoutines.asm ./includes/blockManager.asm ./includes/display.asm
	lwasm --6309 --decb --format=decb --list=./build/ldTiles.lst --symbol-dump=./build/ldTiles.s --symbols --map=./build/ldTiles.map --output=./build/ldTiles.bin ./loadTiles.asm

./build/tileScroll.dsk: ./build/t.bin ./build/ldTiles.bin ./t.bas ./data/CKMAP.DTA ./data/PCBLOX.DAT ./build/sound.bin

ifeq ("$(wildcard ./build/tileScroll.dsk)","")
	decb dskini ./build/tileScroll.dsk
endif
	#../../../lst2cmt.exe /SYSTEM coco3h /OFFSET 21F7 /OVERWRITE ./tileScroll.lst "D:\Emulators\Mame\comments\coco3h.cmt"
	decb copy -0 -a -r ./t.bas ./build/tileScroll.dsk,T.BAS
	decb copy -2 -b -r ./build/t.bin ./build/tileScroll.dsk,T.BIN
	decb copy -2 -b -r ./build/sound.bin ./build/tileScroll.dsk,SOUND.BIN
	decb copy -2 -b -r ./build/ldTiles.bin ./build/tileScroll.dsk,LDTILES.BIN
	decb copy -2 -b -r ./data/CKMAP.DTA ./build/tileScroll.dsk,CKMAP.DTA
	decb copy -2 -b -r ./data/PCBLOX.DAT ./build/tileScroll.dsk,PCBLOX.DAT

clean:
	find ./build -type f \( -name "*.o" -o -name "*.bin" -o -name "*.map" -o -name "*.lst" -o -name "*.s" -o -name "*.dsk" \) -delete
