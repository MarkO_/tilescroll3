10 CLS:PCLEAR 1:CLEAR 100,&H7FFF:GOSUB 300
15 PRINT"1. Load Tiles and Maps and prerender Level 0 first"
20 PRINT"2. Skip loading Tiles and Maps but Prerender Level 0 first"
25 PRINT"3. Skip loading and prerendering all data"
30 PRINT"4. Skip all, just EXEC again (Only if option 1-3 has been ran previously)"
35 PRINT"5. Play Sound Test"
40 OP$=INKEY$:IF OP$="" THEN 40
50 PRINT:OP=INSTR("12345",OP$):IF OP=0 THEN 10
60 ON OP GOTO 100,150,200,210,400
99 '
100 PRINT"Loading...":LOADM"LDTILES.BIN":EXEC
110 PRINT"Loading Cloud Kingdoms ...":LOADM"T.BIN":EXEC
120 GOTO 250
149 '
150 PRINT"Loading Cloud Kingdoms ...":
160 LOADM"T.BIN":EXEC
170 GOTO 250
249 '
200 PRINT"Loading Cloud Kingdoms ..."::LOADM"T.BIN"
210 EXEC:X
220 GOTO 250
249 '
250 PRINT "Done."
260 GOSUB 500
270 END
299 '
300 PALETTE 0,0:PALETTE 8,63:POKE &HFF9A,0:WIDTH 80:RETURN
399 '
400 LOADM"SOUND.BIN":EXEC
410 PRINT PEEK(&H2000)" I, "PEEK(&H2001)" F ("PEEK(&H2002)")"
420 PLAY"L4V31;O3D;O4D;O5D"
430 END
498 '
499 ' PRINT DEBUG OUTPUT
500 PRINT "--- DEBUG OUTPUT ---"
510 PRINT "GIME Check Count:"(PEEK(&H2000)*256+PEEK(&H2001))
520 PRINT "HSync Start:"(PEEK(&H2002)*256+PEEK(&H2003))
530 PRINT "HSync Final:"(PEEK(&H2004)*256+PEEK(&H2005))"(Only useful if score area displayed successfully on exit)"
540 PRINT "Frame Time: "PEEK(&H2006)"v"PEEK(&H2007)"f"
550 PRINT "Debug Word: "RIGHT$("0"+HEX$(PEEK(&H2008)),2)RIGHT$("0"+HEX$(PEEK(&H2009)),2)" ("(PEEK(&H2008)*256+PEEK(&H2009))")"
560 PRINT "Debug Byte: "RIGHT$("0"+HEX$(PEEK(&H200A)),2)" ("PEEK(&H200A)")"
570 PRINT
570 PRINT "--------------------"
580 RETURN
999 '
