    pragma  undefextern
    pragma  newsource
    pragma  cescapes
    pragma  nodollarlocal

    org     $2000

    include includes/display.asm
    include includes/blockManager.asm
    include includes/diskRoutines.asm
    include includes/tileScroll_inc.asm

@version                        fcn "Cloud Kingdoms Data Loader (v1.1)\r"
@dataLoadError                  fcn "Error loading data\r"
start:
    ;
    lbsr    SetFastSpeed
    ldmd    #1
    lbsr    Width80
    lda     #LOGICAL_4000_5FFF
    lbsr    SetSafeMmuScratchBlock
    leax    @version,pcr
    lbsr    PrintString
    lbsr    InitializeDiskIo
    bcc     @initializeDiskIoSuccess
    leax    noDiskSupport,pcr
    lbsr    PrintString
    lbra    @done
@initializeDiskIoSuccess
    lbsr    LoadTiles
    bcs     @error
    lbsr    LoadMaps
    bcc     @done
@error
    leax    @dataLoadError,pcr
    lbsr    PrintString
    ;
@done
    lbsr    UpdateRomCursorPosition
    lbsr    SetSlowSpeed
    ldmd    #0
    rts

endLoadingMessage              fcn '\r'
errorMessageStart              fcn '[%Error $'
errorMessageEnd                fcn ']\r'

@loadingTiles                   fcn "Loading Tiles "
@pcbloxFilename                 fcn 'PCBLOX  DAT'
;***********************
LoadTiles:
;***********************
    pshs    x,b,a
    leax    @loadingTiles,pcr
    lbsr    PrintString
    ;
    leax    PrintProgressIndicator,pcr
    stx     progressCallback
    leax    @pcbloxFilename,pcr
    stx     addressOfFilename
    lda     #TILE_DATA_PHYSICAL_BLOCK
    sta     physicalBlock,pcr
    clrd
    std     physicalOffset,pcr
    lbsr    LoadFileStartingAtPhysicalBlock
    bcs     @error
    bra     @success
    ;
@error
    leax    endLoadingMessage,pcr
    lbsr    PrintString
    coma
    bra     @done
    ;
@success
    leax    endLoadingMessage,pcr
    lbsr    PrintString
    clra
    ;
@done
    puls    a,b,x,pc

@loadingMaps                    fcn "Loading Maps "
@ckmapFilename                  fcn 'CKMAP   DTA'
;***********************
LoadMaps:
;***********************
    pshs    x,b,a
    leax    @loadingMaps,pcr
    lbsr    PrintString
    ;
    leax    PrintProgressIndicator,pcr
    stx     progressCallback
    leax    @ckmapFilename,pcr
    stx     addressOfFilename
    lda     #MAP_DATA_PHYSICAL_BLOCK
    sta     physicalBlock,pcr
    clrd
    std     physicalOffset,pcr
    lbsr    LoadFileStartingAtPhysicalBlock
    bcs     @error
    bra     @success
    ;
@error
    leax    endLoadingMessage,pcr
    lbsr    PrintString
    coma
    bra     @done
    ;
@success
    leax    endLoadingMessage,pcr
    lbsr    PrintString
    clra
    ;
@done
    puls    a,b,x,pc

;***********************
SetFastSpeed:
;***********************
    clr     SetClockSpeedR1_FFD9
    rts

;***********************
SetSlowSpeed:
;***********************
    clr     ClearClockSpeedR1_FFD8
    rts

;***********************
WaitForKey:
;***********************
    pshs    a
@clearKeyStart
    lbsr    IsKeyDownRom
    bne     @clearKeyStart
@pauseStart
    lbsr    IsKeyDownRom
    beq     @pauseStart
    puls    a,pc

;***********************
IsKeyDownRom:
; ```plaintext
; < a = 1 if any key is down, 0 otherwise
; ```
    ; Make ROM call to see if key is pressed
    jsr     [$A000]
    cmpa    #0
    bhi     @keyPressed
    clra                            ; Return 0 for no key down
    bra     @done
@keyPressed
    lda     #1                      ; Return 1 for key down
@done
    rts

    END     start
