    pragma  undefextern
    pragma  newsource
    pragma  cescapes
    pragma  nodollarlocal

    ; compiledSprites.asm loads at Physical $014000 - $015FFF, $016000 - $017FFF, and $018000 - $019FFF
    include includes/compiledSprites.asm

    include includes/structs.asm

    org     $0000
playfieldTopLeftX               rmb     2   ; Current playfield top-left X coordinate
playfieldTopLeftY               rmb     2   ; Current playfield top-left Y coordinate
vsyncCount                      rmb     1
firqCount                       rmb     1
splitScreen                     rmb     1

filler                          rmb     256-(*%256)
waveTable                       rmb     256

    org     $2000

debugData                       rmb     sizeof{DebugData}

    include includes/display.asm
    include includes/blockManager.asm
    include includes/diskRoutines.asm
    include includes/tileScroll_inc.asm

;waveTables                      fdb     squareWaveTable,sawtoothWaveTable,sineWaveTable,midStepWaveTable,triangleWaveTable
;waveTablesCount                 equ     5
squareWaveTable             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

* sineWaveTable               fcb     $7C,$80,$84,$84,$88,$8C,$90,$90,$94,$98,$9C,$9C,$A0,$A4,$A8,$A8
*                             fcb     $AC,$B0,$B4,$B4,$B8,$BC,$BC,$C0,$C4,$C4,$C8,$C8,$CC,$D0,$D0,$D4
*                             fcb     $D4,$D8,$D8,$DC,$DC,$E0,$E0,$E4,$E4,$E8,$E8,$E8,$EC,$EC,$F0,$F0
*                             fcb     $F0,$F0,$F4,$F4,$F4,$F4,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8
*                             fcb     $F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F4,$F4,$F4,$F4,$F0
*                             fcb     $F0,$F0,$EC,$EC,$EC,$E8,$E8,$E4,$E4,$E4,$E0,$E0,$DC,$DC,$D8,$D8
*                             fcb     $D4,$D4,$D0,$CC,$CC,$C8,$C8,$C4,$C0,$C0,$BC,$B8,$B8,$B4,$B0,$AC
*                             fcb     $AC,$A8,$A4,$A4,$A0,$9C,$98,$98,$94,$90,$8C,$88,$88,$84,$80,$7C
*                             fcb     $7C,$78,$74,$70,$70,$6C,$68,$64,$60,$60,$5C,$58,$54,$54,$50,$4C
*                             fcb     $4C,$48,$44,$40,$40,$3C,$38,$38,$34,$30,$30,$2C,$2C,$28,$24,$24
*                             fcb     $20,$20,$1C,$1C,$18,$18,$14,$14,$14,$10,$10,$0C,$0C,$0C,$08,$08
*                             fcb     $08,$04,$04,$04,$04,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
*                             fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$04,$04,$04,$04,$08,$08
*                             fcb     $08,$08,$0C,$0C,$10,$10,$10,$14,$14,$18,$18,$1C,$1C,$20,$20,$24
*                             fcb     $24,$28,$28,$2C,$30,$30,$34,$34,$38,$3C,$3C,$40,$44,$44,$48,$4C
*                             fcb     $50,$50,$54,$58,$5C,$5C,$60,$64,$68,$68,$6C,$70,$74,$74,$78,$7C

* sawtoothWaveTable           fcb     $00,$00,$00,$00,$04,$04,$04,$04,$08,$08,$08,$08,$0C,$0C,$0C,$0C
*                             fcb     $10,$10,$10,$10,$14,$14,$14,$14,$18,$18,$18,$18,$1C,$1C,$1C,$1C
*                             fcb     $20,$20,$20,$20,$24,$24,$24,$24,$28,$28,$28,$28,$2C,$2C,$2C,$2C
*                             fcb     $30,$30,$30,$30,$34,$34,$34,$34,$38,$38,$38,$38,$3C,$3C,$3C,$3C
*                             fcb     $40,$40,$40,$40,$44,$44,$44,$44,$48,$48,$48,$48,$4C,$4C,$4C,$4C
*                             fcb     $50,$50,$50,$50,$54,$54,$54,$54,$58,$58,$58,$58,$5C,$5C,$5C,$5C
*                             fcb     $60,$60,$60,$60,$64,$64,$64,$64,$68,$68,$68,$68,$6C,$6C,$6C,$6C
*                             fcb     $70,$70,$70,$70,$74,$74,$74,$74,$78,$78,$78,$78,$7C,$7C,$7C,$7C
*                             fcb     $80,$80,$80,$80,$84,$84,$84,$84,$88,$88,$88,$88,$8C,$8C,$8C,$8C
*                             fcb     $90,$90,$90,$90,$94,$94,$94,$94,$98,$98,$98,$98,$9C,$9C,$9C,$9C
*                             fcb     $A0,$A0,$A0,$A0,$A4,$A4,$A4,$A4,$A8,$A8,$A8,$A8,$AC,$AC,$AC,$AC
*                             fcb     $B0,$B0,$B0,$B0,$B4,$B4,$B4,$B4,$B8,$B8,$B8,$B8,$BC,$BC,$BC,$BC
*                             fcb     $C0,$C0,$C0,$C0,$C4,$C4,$C4,$C4,$C8,$C8,$C8,$C8,$CC,$CC,$CC,$CC
*                             fcb     $D0,$D0,$D0,$D0,$D4,$D4,$D4,$D4,$D8,$D8,$D8,$D8,$DC,$DC,$DC,$DC
*                             fcb     $E0,$E0,$E0,$E0,$E4,$E4,$E4,$E4,$E8,$E8,$E8,$E8,$EC,$EC,$EC,$EC
*                             fcb     $F0,$F0,$F0,$F0,$F4,$F4,$F4,$F4,$F8,$F8,$F8,$F8,$FC,$FC,$FC,$FC

* midStepWaveTable            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80
*                             fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
*                             fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
*                             fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
*                             fcb     $00,$00,$00,$00
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80
*                             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC

* ; triangle wave
* triangleWaveTable           fcb     $00,$01,$03,$05,$07,$09,$0B,$0D,$0F,$11,$13,$15,$17,$19,$1B,$1D
*                             fcb     $1F,$21,$23,$25,$27,$29,$2B,$2D,$2F,$31,$33,$35,$37,$39,$3B,$3D
*                             fcb     $3F,$40,$42,$44,$46,$48,$4A,$4C,$4E,$50,$52,$54,$56,$58,$5A,$5C
*                             fcb     $5E,$60,$62,$64,$66,$68,$6A,$6C,$6E,$70,$72,$74,$76,$78,$7A,$7C
*                             fcb     $7E,$7F,$81,$83,$85,$87,$89,$8B,$8D,$8F,$91,$93,$95,$97,$99,$9B
*                             fcb     $9D,$9F,$A1,$A3,$A5,$A7,$A9,$AB,$AD,$AF,$B1,$B3,$B5,$B7,$B9,$BB
*                             fcb     $BD,$BE,$C0,$C2,$C4,$C6,$C8,$CA,$CC,$CE,$D0,$D2,$D4,$D6,$D8,$DA
*                             fcb     $DC,$DE,$E0,$E2,$E4,$E6,$E8,$EA,$EC,$EE,$F0,$F2,$F4,$F6,$F8,$FA
*                             fcb     $FC,$FA,$F8,$F6,$F4,$F2,$F0,$EE,$EC,$EA,$E8,$E6,$E4,$E2,$E0,$DE
*                             fcb     $DC,$DA,$D8,$D6,$D4,$D2,$D0,$CE,$CC,$CA,$C8,$C6,$C4,$C2,$C0,$BE
*                             fcb     $BD,$BB,$B9,$B7,$B5,$B3,$B1,$AF,$AD,$AB,$A9,$A7,$A5,$A3,$A1,$9F
*                             fcb     $9D,$9B,$99,$97,$95,$93,$91,$8F,$8D,$8B,$89,$87,$85,$83,$81,$7F
*                             fcb     $7E,$7C,$7A,$78,$76,$74,$72,$70,$6E,$6C,$6A,$68,$66,$64,$62,$60
*                             fcb     $5E,$5C,$5A,$58,$56,$54,$52,$50,$4E,$4C,$4A,$48,$46,$44,$42,$40
*                             fcb     $3F,$3D,$3B,$39,$37,$35,$33,$31,$2F,$2D,$2B,$29,$27,$25,$23,$21
*                             fcb     $1F,$1D,$1B,$19,$17,$15,$13,$11,$0F,$0D,$0B,$09,$07,$05,$03,$01

scoreTemplate               fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00 
                            fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, 'C,$32, 'l,$32, 'o,$32, 'u,$32, 'd,$32, ' ,$32, '0,$18, '0,$18, ' ,$32, 'K,$32, 'i,$32, 'n,$32, 'g,$32, 'd,$32, 'o,$32, 'm,$32, 's,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$00, ' ,$00, ' ,$00, ' ,$00 
                            fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$00, ' ,$00, ' ,$00 
                            fcb     ' ,$00, ' ,$00, ' ,$0F, ' ,$0F, 'G,$0F, 'e,$0F, 'm,$0F, 's,$0F, ':,$0F, '0,$18, '0,$18, ' ,$0F, ' ,$0F, ' ,$0F, 'S,$0F, 'c,$0F, 'o,$0F, 'r,$0F, 'e,$0F, ':,$0F, '0,$18, '0,$18, '0,$18, '0,$18, '0,$18, '0,$18, '0,$18, ' ,$0F, ' ,$0F, 'K,$0F, 'e,$0F, 'y,$0F, 's,$0F, ':,$0F, '0,$18, '0,$18, ' ,$0F, ' ,$0F, ' ,$00, ' ,$00 
                            fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$00, ' ,$00, ' ,$00 
                            fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$0F, ' ,$0F, ' ,$0F, '>,$0F, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$0F, ' ,$0F, ' ,$0F, ' ,$00, ' ,$00, ' ,$00, ' ,$00
                            fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00 

    include includes/mapLookups.asm
    include includes/tunes.asm

tunes.currentTuneIndex      fcb     $00         ; Index into tunes.addresses and tunes.lengths of current tune playing
tunes.toneInTuneAddress     fdb     $0000       ; address of current tone in current tune
tunes.durationRemaining     fcb     $00         ; Current tone's duration remaining before we move to the next tone
tunes.tonesRemaining        fcb     $00         ; Number of tones remaining in the current tune before we stop playing

shouldSkipInitialPrerenderLevel fcb 0   ; 0 = Prerender level 0 on start up, != 0 = Skip prerendering level 0 on start up
bcdTimer                    fcb     $00
bcdGems                     fcb     $00
bcdKeys                     fcb     $00
bcdScore                    fqb     $00000000
health                      fcb     100
paletteStore                rmb     16
keyColumnScans              fcb     1,1,1,1,1,1,1,1
previousKeyColumnScans      fcb     1,1,1,1,1,1,1,1

powerUpCompiledSprites      fdb     PowerUps.Key,PowerUps.StopWatch,PowerUps.Soda,PowerUps.Wings,PowerUps.Shield,PowerUps.Gem,PowerUps.Paint
currentMap                  rmb     512
currentWaveTable            fcb     $00
shouldAccelerateUp          fcb     0
shouldAccelerateDown        fcb     0
shouldAccelerateLeft        fcb     0
shouldAccelerateRight       fcb     0
shouldQuit                  fcb     0
shouldJump                  fcb     0
shouldCycleToNextLevel      fcb     0
shouldCycleToPreviousLevel  fcb     0
* shouldCycleToNextWaveTable  fcb     0
shouldDebugLT               fcb     0
shouldDebugGT               fcb     0
currentLevel                fcb     $00
mapXHeroIsOn                fdb     $0000
mapYHeroIsOn                fdb     $0000
addressOfMapCellHeroIsOn    fdb     $0000
actionIndexForMapCellHeroIsOn fcb   $00


start:
;***********************
    lbsr    Width32
    lbsr    SetHsyncCountForGimeVersion
    lbsr    Width80
    lbsr    InitializeParameters
    lbsr    InitializeDiskIo
    lbsr    InitializeScreenBuffers
    bcc     @initializeDiskIoSuccess
    leax    noDiskSupport,pcr
    lbsr    PrintString
    lbra    @done
@initializeDiskIoSuccess
    lbsr    SetFastSpeed
    ldmd    #1
    lbsr    Set80x24TextMode
    leax    @version,pcr
    lbsr    PrintString
    * leax    @romsInStartingTakeover,pcr
    * lbsr    PrintString
    ;
    orcc    #%01010000
    lbsr    SwapRomsOut
    lda     #LOGICAL_6000_7FFF
    lbsr    SetSafeMmuScratchBlock
    andcc   #%10101111
    * leax    @romsOutAboutToCallTests,pcr
    * lbsr    PrintString
    ;
    lbsr    InitializeSound
    lbsr    StartGameLoop
    lbsr    UninitializeSound
    * leax    @romsOutDoneWithTests,pcr
    * lbsr    PrintString
    ;
@doneWithTest
    orcc    #%01010000
    lbsr    SwapRomsIn
    andcc   #%10101111
    lbsr    Set80x24TextMode
    * leax    @romsInDoneWithTakeover,pcr
    * lbsr    PrintString
    ldd     highSpeedTimerCount+1,pcr
    std     debugData+DebugData.highSpeedTimerCountFinal
@done
    lbsr    ResetRgbPalette
    lbsr    SetSlowSpeed
    ldmd    #0
    rts
@version                        fcn "Cloud Kingdoms\rVersion 1.5\r"
@dataLoadError                  fcn "Error loading data\r"
* @romsInStartingTakeover         fcn "Roms in, starting takeover\r"
* @romsOutAboutToCallTests        fcn "Roms Out, about to call tests\r"
* @romsOutDoneWithTests           fcn "Roms Out, done with tests\r"
* @romsInDoneWithTakeover         fcn "Roms In, done with takeover\r"

InitializeParameters
;***********************
    pshs    b,a
    clr     printChar80x24_CurrentCursorX,pcr
    clr     printChar80x24_CurrentCursorY,pcr
    ;
    jsr     GetCurrentBasicCharacter
    cmpa    #':
    bne     @done
@ParameterLoop
    jsr     GetNextBasicCharacter
    beq     @done               ; Next character is 0, so we hit the end of the line.
    bsr     ProcessParameter
    bra     @ParameterLoop
@done
    puls    a,b,pc

ProcessParameter
; ```plaintext
; > a = parameter character
; ```
;***********************
    cmpa    #'X                 ; X = skip data
    bne     @done
    sta     shouldSkipInitialPrerenderLevel,pcr
@done
    rts

InitializeSound
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    aim     #%11110111;PIA0SideAControlRegister_FF01                        ; MSB & LSB of MUX set to 0 = Select DAC (Digital to Analog Converter) as sound source
    ;
    ;         PIA 0 Side B Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    aim     #%11110111;PIA0SideBControlRegister_FF03                        ; MSB & LSB set to 0 = Select DAC (Digital to Analog Converter) as sound source
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    oim     #%00001000;PIA1SideBControlRegister_FF23                        ; Enable Sound
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    aim     #%11111011;PIA1SideAControlRegister_FF21                        ; Make $FF20 configure direction for DAC, RS-323C, and Cassette
    ;
    ;         PIA 1 Side A Control Data Register
    ;         ┌┬┬┬┬┬───── 6-Bit DAC
    ;         ││││││┌──── Bit  1   - RS-232C Data Output
    ;         │││││││┌─── Bit  0   - Cassette Data Input
    lda     #%11111100
    sta     PIA1SideADataRegister_FF20                                      ; Set DAC to Output; Set RS-232C and Cassette to Input (so writes to those two bits do nothing)
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    oim     #%00000100;PIA1SideAControlRegister_FF21                        ; Make $FF20 function as input/output again
    rts

UninitializeSound
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    aim     #%11111011;PIA1SideAControlRegister_FF21                        ; Make $FF20 configure direction for DAC, RS-323C, and Cassette
    ;
    ;         PIA 1 Side A Control Data Register
    ;         ┌┬┬┬┬┬───── 6-Bit DAC
    ;         ││││││┌──── Bit  1   - RS-232C Data Output
    ;         │││││││┌─── Bit  0   - Cassette Data Input
    lda     #%11111110
    sta     PIA1SideADataRegister_FF20                                      ; DAC/RS-232C to Output, Cassette to Input
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    aim     #%11110111;PIA1SideBControlRegister_FF23                        ; Disable Sound
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    oim     #%00000100;PIA1SideAControlRegister_FF21                        ; Make $FF20 function as input/output again
    lda     #%00000010
    sta     PIA1SideADataRegister_FF20                                      ; Set RS-232C idle
    rts

InitializeScreenBuffers
    pshs    b,a
    ;
    clrd
    std     screenBuffers+ScreenBuffer.verticalOffset,pcr
    std     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.verticalOffset,pcr
    sta     screenBuffers+ScreenBuffer.leftPlayfieldXInSlice,pcr
    sta     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.leftPlayfieldXInSlice,pcr
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK
    sta     screenBuffers+ScreenBuffer.firstPhysicalBlockOfBuffer,pcr
    lda     #BUFFER2_FIRST_PHYSICAL_BLOCK
    sta     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.firstPhysicalBlockOfBuffer,pcr
    lda     #BUFFER1_VIDEO_BANK
    sta     screenBuffers+ScreenBuffer.videoBank,pcr
    lda     #BUFFER2_VIDEO_BANK
    sta     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.videoBank,pcr
    ;
    puls    a,b,pc


Set80x24TextMode
;***********************
    pshs    a
    ;         Video Mode
    ;         ┌────────── Bit  0   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%00000011
    sta     GIME_VideoMode_FF98
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    lda     #%00000000
    sta     GIME_VideoBankSelect_FF9B
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (Graphics: 000=16,001=20,010=32,011=40,100=64,101=80,110=128,111=160 bytes per row, Text: 0x0=32,0x1=40,1x0=64,1x1=80)
    ;         ││││││┌┬─── Bits 1-0 - CRES (Graphics: 00=2, 01=4, 10=16, 11=undefined colors, Text: x0=No color attributes, x1=Color attributes)
    lda     #%00010101
    sta     GIME_VideoResolution_FF99
    ;
    ;         Border Color
    ;         ┌┬───────── Bits 7-6 - Unused
    ;         ││┌──┬───── Bits 5,2 - Red
    ;         │││┌─│┬──── Bits 4,1 - Green
    ;         ││││┌││┬─── Bits 3,0 - Blue
    lda     #%00000000
    sta     GIME_BorderColor_FF9A
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    lda     #%00000000
    sta     GIME_VideoBankSelect_FF9B
    ;
    ;         Vertical Scroll
    ;         ┌┬┬┬─────── Bits 7-4 - Unused
    ;         ││││┌┬┬┬─── Bits 3-0 - VSC (Vertical Scroll Register for smooth scrolling in text modes)
    lda     #%00000000
    sta     GIME_VerticalScroll_FF9C
    ;
    ;         Vertical Offset 1
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 18-11 of 19 bit Physical address for start of display
    lda     #%11011000
    sta     GIME_VerticalOffset1_FF9D
    ;
    ;         Vertical Offset 0
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 10-3 of 19 bit Physical address for start of display (bits 2-0 are always 0)
    lda     #%00000000  ; %11011000_00000000_000 = &h6C000
    sta     GIME_VerticalOffset0_FF9E
    ;
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen Enable
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%00000000
    sta     GIME_HorizontalOffset_FF9F
    ;
    puls    a,pc

Set320x200x16_GraphicsMode
;***********************
    pshs    x,a
    ;         VideoMode
    ;         ┌────────── Bit  0   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%10000001
    sta     GIME_VideoMode_FF98
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (Graphics: 000=16,001=20,010=32,011=40,100=64,101=80,110=128,111=160 bytes per row, Text: 0x0=32,0x1=40,1x0=64,1x1=80)
    ;         ││││││┌┬─── Bits 1-0 - CRES (Graphics: 00=2, 01=4, 10=16, 11=undefined colors, Text: x0=No color attributes, x1=Color attributes)
    lda     #%00111110
    sta     GIME_VideoResolution_FF99
    ;
    ;         Border Color
    ;         ┌┬───────── Bits 7-6 - Unused
    ;         ││┌──┬───── Bits 5,2 - Red
    ;         │││┌─│┬──── Bits 4,1 - Green
    ;         ││││┌││┬─── Bits 3,0 - Blue
    lda     #%00000000
    sta     GIME_BorderColor_FF9A
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    ;       #%00000000
    ldx     onScreenBuffer,pcr
    lda     ScreenBuffer.videoBank,x
    sta     GIME_VideoBankSelect_FF9B
    ;
    ;         Vertical Scroll
    ;         ┌┬┬┬─────── Bits 7-4 - Unused
    ;         ││││┌┬┬┬─── Bits 3-0 - VSC (Vertical Scroll Register for smooth scrolling in text modes)
    lda     #%00000000
    sta     GIME_VerticalScroll_FF9C
    ;
    ;         Vertical Offset 1
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 18-11 of 19 bit Physical address for start of display
    ;       #%00011100  ; %00011100_00000000_000 = %000_1000_0000_0000_0000 = &h0E000
    lda     ScreenBuffer.verticalOffset,x
    sta     GIME_VerticalOffset1_FF9D
    ;
    ;         Vertical Offset 0
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 10-3 of 19 bit Physical address for start of display (bits 2-0 are always 0)
    ;       #%00000000  ; %00011100_00000000_000 = %000_1000_0000_0000_0000 = &h0E000
    lda     ScreenBuffer.verticalOffset+1,x  ; %00000000_00000000_000 = %000_0000_0000_0000_0000 = &h00000
    sta     GIME_VerticalOffset0_FF9E
    ;
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen: 1 = Enabled, 0 = Disabled
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%00000000
    sta     GIME_HorizontalOffset_FF9F
    ;
    puls    a,x,pc

Enable256ByteWideMode
;***********************
    pshs    a
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen: 1 = Enabled, 0 = Disabled
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%10000000
    sta     GIME_HorizontalOffset_FF9F
    puls    a,pc

SetupInterruptSources
;***********************
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    aim     #%11111110;PIA0SideAControlRegister_FF01    ; Disable HSYNC IRQ
    ;
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    aim     #%11111110;PIA0SideBControlRegister_FF03    ; Disable VSYNC IRQ
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    aim     #%11111110;PIA1SideAControlRegister_FF21    ; Disable Cassette Data FIRQ
    ;
    ;        PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    aim     #%11111110;PIA1SideBControlRegister_FF23    ; Disable Cartridge FIRQ
    ;
    ;         Initialization Register 0 - INIT0 
    ;         ┌────────── Bit  7   - CoCo Bit (0 = CoCo 3 Mode, 1 = CoCo 1/2 Compatible)
    ;         │┌───────── Bit  6   - M/P (1 = MMU enabled, 0 = MMU disabled)
    ;         ││┌──────── Bit  5   - IEN (1 = GIME IRQ output enabled to CPU, 0 = disabled)
    ;         │││┌─────── Bit  4   - FEN (1 = GIME FIRQ output enabled to CPU, 0 = disabled)
    ;         ││││┌────── Bit  3   - MC3 (1 = Vector RAM at FEXX enabled, 0 = disabled)
    ;         │││││┌───── Bit  2   - MC2 (1 = Standard SCS (DISK) (0=expand 1=normal))
    ;         ││││││┌┬─── Bits 1-0 - MC1-0 (10 = ROM Map 32k Internal, 0x = 16K Internal/16K External, 11 = 32K External - Except Interrupt Vectors)
    lda     #%01111110
    sta     GIME_InitializeRegister0_FF90               ; CoCo 3 Mode; Enable MMU, IRQ, FIRQ; Vector RAM at $FFEx; ROM 32k internal
    ;
    ;         Initialization Register 1 - INIT1
    ;         ┌────────── Bit  7   - Unused
    ;         │┌───────── Bit  6   - Memory type (1=256K, 0=64K chips)
    ;         ││┌──────── Bit  5   - Timer input clock source (1=279.365 nsec, 0=63.695 usec)
    ;         │││┌┬┬┬──── Bits 4-1 - Unused
    ;         │││││││┌─── Bits 0   - MMU Task Register select (0=enable $FFA0-$FFA7, 1=enable $FFA8-$FFAF)
    lda     #%00100000
    sta     GIME_InitializeRegister1_FF91               ; Timer 63.695 usec (*10^-6, low speed); MMU $FFA0-$FFA7 task selected
    ;
    ;         Interrupt Request Enable Register - IRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00001000
    sta     GIME_InterruptReqEnable_FF92        ; Enable IRQS: VSYNC
                                                ; Disable IRQs: Timer, HSYNC, RS232, Keyboard, Cartridge
    ;
    ;         Fast Interrupt Request Enable Register - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00000000
    sta     GIME_FastInterruptReqEnable_FF93    ; Disable FIRQS: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    lbsr    SetupInterruptVectors
    rts

EnableSplitScreen
    pshs    a
    ;
    clr     <splitScreen
    inc     <splitScreen
    ;
    puls    a,pc

DisableSplitScreen
    pshs    a
    ;
    clr     <splitScreen
    ;
    puls    a,pc

RestoreInterruptSources
;***********************
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    oim     #%00000001;PIA0SideBControlRegister_FF03    ; Enable VSYNC IRQ
    ;
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    aim     #%11111110;PIA0SideAControlRegister_FF01    ; Disable HSYNC IRQ
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    aim     #%11111110;PIA1SideAControlRegister_FF21    ; Disable the Cassette Data FIRQ
    ;
    ;        PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    aim     #%11111110;PIA1SideBControlRegister_FF23    ; Disable the Cartridge FIRQ
    ;
    ;         Initialization Register 0 - INIT0 
    ;         ┌────────── Bit  7   - CoCo Bit (0 = CoCo 3 Mode, 1 = CoCo 1/2 Compatible)
    ;         │┌───────── Bit  6   - M/P (1 = MMU enabled, 0 = MMU disabled)
    ;         ││┌──────── Bit  5   - IEN (1 = GIME IRQ output enabled to CPU, 0 = disabled)
    ;         │││┌─────── Bit  4   - FEN (1 = GIME FIRQ output enabled to CPU, 0 = disabled)
    ;         ││││┌────── Bit  3   - MC3 (1 = Vector RAM at FEXX enabled, 0 = disabled)
    ;         │││││┌───── Bit  2   - MC2 (1 = Standard SCS (DISK) (0=expand 1=normal))
    ;         ││││││┌┬─── Bits 1-0 - MC1-0 (10 = ROM Map 32k Internal, 0x = 16K Internal/16K External, 11 = 32K External - Except Interrupt Vectors)
    lda     #%01001110
    sta     GIME_InitializeRegister0_FF90
    ;
    ;         Fast/Interrupt Request Enable Registers - IRQENR/FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00000000
    sta     GIME_InterruptReqEnable_FF92        ; Disable IRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    sta     GIME_FastInterruptReqEnable_FF93    ; Disable FIRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    ;
    lda     GIME_InterruptReqEnable_FF92
    lda     GIME_FastInterruptReqEnable_FF93
    lbsr    RestoreInterruptVectors
    rts

endMessage              fcn     ".\r"

WaitForKeyEndMessageNoRom
;***********************
@clearKeyStart
    lbsr    IsKeyDownNoRom
    bne     @clearKeyStart
@pauseStart
    lbsr    IsKeyDownNoRom
    beq     @pauseStart
    leax    endMessage,pcr
    lbsr    PrintString
    rts

WaitForKeyEndMessageRom
@clearKeyStart
    lbsr    IsKeyDownRom
    bne     @clearKeyStart
@pauseStart
    lbsr    IsKeyDownRom
    beq     @pauseStart
    leax    endMessage,pcr
    lbsr    PrintString
    rts

romInStack              rmb 2
romOutStack             fdb $600
zeroPageInitialized     fcb 0

SwapRomsOut
;***********************
    pshs    x,b,a
    ;
    sta     RamMode_FFDF                ; Switch $8xxx-$Fxxx RAM mode
    ;
    ldb     #PHYSICAL_060000_061FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_0000_1FFF
    leax    mmu+LOGICAL_0000_1FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_068000_069FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_8000_9FFF
    leax    mmu+LOGICAL_8000_9FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_06A000_06BFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    leax    mmu+LOGICAL_A000_BFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_06C000_06DFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    mmu+LOGICAL_C000_DFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_06E000_06FFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_E000_FFFF
    leax    mmu+LOGICAL_E000_FFFF,pcr
    stb     ,x
    ;
    ;                           ; $7F50 = old_a, $7F51 = old_b, $7F52-3 = old_x, $7F54-5 = return
    tfr     s,x                 ; s = $7F50, x = $7F50
    leas    6,s                 ; s = $7F56
    sts     romInStack,pcr      ; romInStack = $7F56
    lds     romOutStack,pcr     ; s = $0600
    ldd     4,x                 ; d = return
    pshs    b,a                 ; $05FE-F = return, s = $05FE
    ldd     ,x                  ; d = old_d
    ldx     2,x                 ; x = old_x
    ;
    ldb     #PHYSICAL_066000_067FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_6000_7FFF
    leax    mmu+LOGICAL_6000_7FFF,pcr
    stb     ,x
    ;
    ldb     #LOGICAL_6000_7FFF
    stb     safeMmuLogicalBlock,pcr
    ldx     #$6000
    stx     safeMmuLogicalBlockAddress,pcr
    ;
    tst     zeroPageInitialized,pcr
    bne     @zeroPageInitialized
    lbsr    DisableSplitScreen
    clr     debugData+DebugData.debugWord
    clr     debugData+DebugData.debugWord+1
    ldx     #squareWaveTable
    bsr     PopulateWaveTable
    * ldd     #$FFFF
    * sta     <soundTest
    * sta     <soundTest+2
    * clrd
    * sta     <soundTest+4
    * sta     <soundTest+6
    * lda     #8
    * sta     soundIndex
    inc     zeroPageInitialized,pcr
@zeroPageInitialized
    lbsr    SetupInterruptSources
    ;
    rts

PopulateWaveTable
; ```plaintext
; > x = address of wave table to use
; ```
;***********************
;
    ldy     #waveTable
    ldw     #256
    tfm     x+,y+
    ;
    rts

SwapRomsIn
;***********************
    pshs    x,b,a
    ;
    lbsr    RestoreInterruptSources
    ;
    ldb     #PHYSICAL_076000_077FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_6000_7FFF
    leax    mmu+LOGICAL_6000_7FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_078000_079FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_8000_9FFF
    leax    mmu+LOGICAL_8000_9FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_07A000_07BFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    leax    mmu+LOGICAL_A000_BFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_07C000_07DFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    mmu+LOGICAL_C000_DFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_07E000_07FFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_E000_FFFF
    leax    mmu+LOGICAL_E000_FFFF,pcr
    stb     ,x
    ;
    lda     #LOGICAL_4000_5FFF
    lbsr    SetSafeMmuScratchBlock
    ;
    ;                           ; $05FA = old_a, $05FB = old_b, $05FC-D = old_x, $05FE-F = return
    tfr     s,x                 ; s = $05FA, x = $05FA
    leas    6,s                 ; s = $0600
    sts     romOutStack,pcr     ; romOutStack = $0600
    lds     romInStack,pcr      ; s = $7F56
    ldd     4,x                 ; d = return
    pshs    b,a                 ; $7F54-5 = return, s = $7F54
    ldd     ,x                  ; x = old_x
    ldx     2,x                 ; d = old_d
    ;
    pshs    x,b
    ldb     #PHYSICAL_070000_071FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_0000_1FFF
    leax    mmu+LOGICAL_0000_1FFF,pcr
    stb     ,x
    puls    b,x
    ;
    lbsr    UpdateRomCursorPosition
    ;
    rts

StartGameLoop:
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    lbsr    SavePalette
    clrd
    sta     currentLevel,pcr
    std     bcdScore,pcr
    std     bcdScore+2,pcr
    ; DEBUG - Set test values for Timer, Games, Keys, and Score.
    lda     #$45
    sta     bcdTimer,pcr
    lda     #$89
    sta     bcdGems,pcr
    lda     #$23
    sta     bcdKeys,pcr
    ldq     #$01234567
    stq     bcdScore,pcr
    ; DEBUG - Subtract BCD 12 from each of the values.
    lda     #$12
    lbsr    SubtractFromBcdScore
    leax    bcdTimer,pcr
    lbsr    SubtractFromSingleByteBcd
    leax    bcdGems,pcr
    lbsr    SubtractFromSingleByteBcd
    leax    bcdKeys,pcr
    lbsr    SubtractFromSingleByteBcd
@levelLoop
    lbsr    DisableSplitScreen
    lbsr    InitializeLevel
    tst     shouldSkipInitialPrerenderLevel,pcr
    beq     @continue
    clr     shouldSkipInitialPrerenderLevel,pcr
    bra     @donePrerenderingVerticalSlices
@continue
    lbsr    DisplayLevelLoadingScreen
    * lbsr    PrenderAllTilesForDebug
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK
    clre
    clrb
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #4
    lbsr    PrerenderLevel256ByteVerticalSlice
    ;
    clre
    adda    #16
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #4
    lbsr    PrerenderLevel256ByteVerticalSlice
    ;
@donePrerenderingVerticalSlices
    lbsr    WaitForVsync
    ;
    lbsr    SetEgaPalette
    lbsr    Clear80x24Line
    lbsr    Set320x200x16_GraphicsMode
    lbsr    Enable256ByteWideMode
    lbsr    EnableSplitScreen
    clr     <vsyncCount
    clra
@startLoop
    ;
    lbsr    ProcessGameKeys
    ;
    tst     sprites+Sprite.speedX,pcr
    beq     @doneApplyingHorizontalFriction
    blt     @applyPositiveHorizontalFriction
    dec     sprites+Sprite.speedX,pcr
    ;dec     sprites+Sprite.speedX,pcr
    bra     @doneApplyingHorizontalFriction
@applyPositiveHorizontalFriction
    inc     sprites+Sprite.speedX,pcr
    ;inc     sprites+Sprite.speedX,pcr
@doneApplyingHorizontalFriction
    tst     sprites+Sprite.speedY,pcr
    beq     @doneApplyingVerticalFriction
    blt     @applyPositiveVerticalFriction
    dec     sprites+Sprite.speedY,pcr
    ;dec     sprites+Sprite.speedY,pcr
    bra     @doneApplyingVerticalFriction
@applyPositiveVerticalFriction
    inc     sprites+Sprite.speedY,pcr
    ;inc     sprites+Sprite.speedY,pcr
@doneApplyingVerticalFriction
@checkAccelerateRight
    tst     shouldAccelerateRight,pcr
    beq     @checkAccelerateLeft
    lda     sprites+Sprite.speedX,pcr
    adda    #2
    cmpa    #8
    ble     @doneUpperCapHorizontalSpeed
    lda     #8
@doneUpperCapHorizontalSpeed
    sta     sprites+Sprite.speedX,pcr
@checkAccelerateLeft
    tst     shouldAccelerateLeft,pcr
    beq     @checkAccelerateDown
    lda     sprites+Sprite.speedX,pcr
    suba    #2
    cmpa    #-8
    bge     @doneLowerCapHorizontalSpeed
    lda     #-8
@doneLowerCapHorizontalSpeed
    sta     sprites+Sprite.speedX,pcr
@checkAccelerateDown
    tst     shouldAccelerateDown,pcr
    beq     @checkAccelerateUp
    lda     sprites+Sprite.speedY,pcr
    adda    #2
    cmpa    #8
    ble     @doneUpperCapVerticalSpeed
    lda     #8
@doneUpperCapVerticalSpeed
    sta     sprites+Sprite.speedY,pcr
@checkAccelerateUp
    tst     shouldAccelerateUp,pcr
    beq     @doneCheckAccelerate
    lda     sprites+Sprite.speedY,pcr
    suba    #2
    cmpa    #-8
    bge     @doneLowerCapVerticalSpeed
    lda     #-8
@doneLowerCapVerticalSpeed
    sta     sprites+Sprite.speedY,pcr
@doneCheckAccelerate
    ;
    lda     sprites+Sprite.speedX,pcr
    ldx     sprites+Sprite.x
    leax    a,x
    cmpx    #0
    bge     @doneLowerCapHeroX
    ldx     #0
    clr     sprites+Sprite.speedX,pcr
@doneLowerCapHeroX
    cmpx    #1024-32
    ble     @doneUpperCapHeroX
    ldx     #1024-32
    clr     sprites+Sprite.speedX,pcr
@doneUpperCapHeroX
    stx     sprites+Sprite.x
    leax    -(32*5),x
    cmpx    #0
    bge     @doneLowerCapPlayfieldX
    ldx     #0
@doneLowerCapPlayfieldX
    cmpx    #1024-320
    ble     @doneUpperCapPlayfieldX
    ldx     #1024-320
@doneUpperCapPlayfieldX
    stx     <playfieldTopLeftX
    ;
    lda     sprites+Sprite.speedY,pcr
    ldx     sprites+Sprite.y
    leax    a,x
    cmpx    #0
    bge     @doneLowerCapHeroY
    ldx     #0
    clr     sprites+Sprite.speedY,pcr
@doneLowerCapHeroY
    cmpx    #512-32-1
    ble     @doneUpperCapHeroY
    ldx     #512-32-1
    clr     sprites+Sprite.speedY,pcr
@doneUpperCapHeroY
    stx     sprites+Sprite.y
    leax    -(32*2),x
    cmpx    #0
    bge     @doneLowerCapPlayfieldY
    ldx     #0
@doneLowerCapPlayfieldY
    cmpx    #512-144-1
    ble     @doneUpperCapPlayfieldY
    ldx     #512-144-1
@doneUpperCapPlayfieldY
    stx     <playfieldTopLeftY
    ;
@checkJump
    tst     shouldJump,pcr
    beq     @doneCheckJump
    tst     currentJumpIndex,pcr
    bne     @doneCheckJump
    lda     #25
    sta     currentJumpIndex,pcr
    lda     #tunes.jump.index
    lbsr    PlayTune
    ;
@doneCheckJump
    lbsr    UpdateGameState
    lbsr    UpdateScreen
    lda     <vsyncCount
    sta     debugData+DebugData.lastFrameVsyncCount
    lda     <firqCount
    sta     debugData+DebugData.lastFrameFIRQCount
    lda     actionIndexForMapCellHeroIsOn
    sta     debugData+DebugData.debugByte
    ;lda     #%00000000      ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    ;sta     GIME_BorderColor_FF9A
    ;
@timingDelay
    lbsr    WaitForVsync
    lda     <vsyncCount
    cmpa    #3
    blt     @timingDelay
    clr     <vsyncCount
    ;lda     #%00100100      ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    ;sta     GIME_BorderColor_FF9A
    lbsr    SwapVideoBuffers
    lbsr    RestoreOffScreenSpriteBackgrounds
    lbsr    ProgressTune
@checkQuit
    tst     shouldQuit,pcr
    lbne    @quitGame
@checkCycleNextLevel
    tst     shouldCycleToNextLevel,pcr
    beq     @checkCyclePreviousLevel
    lda     currentLevel,pcr
    inca
    anda    #$1F
    sta     currentLevel,pcr
    lbra    @levelLoop
@checkCyclePreviousLevel
    tst     shouldCycleToPreviousLevel,pcr
    beq     @checkDebugLT
    lda     currentLevel,pcr
    deca
    anda    #$1F
    sta     currentLevel,pcr
    lbra    @levelLoop
* @checkCycleNextWaveTable
*     tst     shouldCycleToNextWaveTable,pcr
*     beq     @checkDebugLT
*     lda     currentWaveTable,pcr
*     inca
*     cmpa    #waveTablesCount
*     blt     @copyWaveTable
*     clra
* @copyWaveTable
*     sta     currentWaveTable,pcr
*     ldx     #waveTables
*     lsla
*     ldx     a,x
*     lbsr    PopulateWaveTable
*     lbsr    WaitForVsync
*     lbsr    WaitForVsync
*     lbsr    WaitForVsync
*     lbsr    WaitForVsync
*     lbsr    WaitForVsync
*     lbsr    WaitForVsync
@checkDebugLT
    tst     shouldDebugLT,pcr
    beq     @checkDebugGT
    ldd     highSpeedTimerCount+1,pcr
    incd
    std     highSpeedTimerCount+1
@checkDebugGT
    tst     shouldDebugGT,pcr
    beq     @doneCheckingKeys
    ldd     highSpeedTimerCount+1,pcr
    decd
    std     highSpeedTimerCount+1,pcr
@doneCheckingKeys
    lbra    @startLoop
    ;
@quitGame
    lbsr    ClearGameKeys
    lbsr    SwapVideoBuffers
    lbsr    RestoreOffScreenSpriteBackgrounds
    lbsr    RestorePalette
    ;
@done
    pulsw
    puls    a,b,x,y,pc

UpdateGameState
    lbsr    GetHerosCurrentMapCellData
    leax    heroActionLookup,pcr
    jsr     [a,x]
    rts

UpdateScreen
;***********************
    lbsr    UpdateScroll
    lbsr    UpdateScore
    lbsr    ProgressJump
    lbsr    UpdateEnemies
    lbsr    UpdateSprites
    lbsr    SaveOffScreenSpriteBackgrounds
    lbsr    DrawOffScreenSprites
    lbsr    CyclePalettes
    ;
    rts

CyclePalettes
    pshs    x,b,a
    ;
    inc     @paletteCycleTimer,pcr
    lda     @paletteCycleTimer,pcr
    bita    #1
    beq     @done
    ;
    ldx     #ColorPaletteFirstRegister_FFB0
    lda     5,x
    ldb     13,x
    stb     5,x
    sta     13,x
    ;
@done
    puls    a,b,x,pc
@paletteCycleTimer          fcb 0

jumpHeights                 fcb 0,1,1,0,1,2,3,3,2,1,0,1,2,3,3,3,4,4,4,4,3,3,3,2,1
currentJumpIndex            fcb 0
currentJumpHeight           fdb 0

ProgressJump
    pshs    x,a
    ;
    tst     currentJumpIndex,pcr
    beq     @done
    dec     currentJumpIndex,pcr
    leax    jumpHeights,pcr
    lda     currentJumpIndex,pcr
    lda     a,x
    sta     currentJumpHeight+1,pcr
    bne     @done
    lda     #tunes.land.index
    lbsr    PlayTune
@done
    ;
    puls    a,x,pc

spriteCount                 equ 6
spriteAddresses             fdb sprites,sprites+sizeof{Sprite},sprites+sizeof{Sprite}*2,sprites+sizeof{Sprite}*3,sprites+sizeof{Sprite}*4,sprites+sizeof{Sprite}*5

EnemyDemo           STRUCT
startX                      rmb 2
startY                      rmb 2
minX                        rmb 2
maxX                        rmb 2
minY                        rmb 2
maxY                        rmb 2
startSpeedX                 rmb 1
startSpeedY                 rmb 1
                    ENDSTRUCT

enemyDemos
;                                   startX, startY, minX, maxX, minY, maxY, startSpeedX/Y
                            fdb     256,    96,     256,  400,  96,   96,   $0400
                            fdb     32,     64,     32,   32,   32,   256,  $0004
                            fdb     32,     32,     32,   256,  32,   256,  $0404
                            fdb     256,    32,     32,   256,  32,   256,  $FC04
                            fdb     32,     96,     32,   288,  32,   111,  $0404

UpdateEnemies
    pshs    y,x,b,a
    pshsw
    ;
    leax    enemyDemos,pcr
    leay    sprites+sizeof{Sprite},pcr
    clra
    inca
    pshs    a
@enemyLoop
    ;
    ldw     Sprite.x,y
    ldb     Sprite.speedX,y
    sex
    addr    d,w
    ldd     EnemyDemo.maxX,x
    cmpr    d,w
    bge     @flipXSpeed
    cmpw    EnemyDemo.minX,x
    ble     @flipXSpeed
    bra     @doneCapXSpeed
@flipXSpeed
    neg     Sprite.speedX,y
@doneCapXSpeed
    stw     Sprite.x,y
    ;
    ldw     Sprite.y,y
    ldb     Sprite.speedY,y
    sex
    addr    d,w
    cmpw    EnemyDemo.maxY,x
    bge     @flipYSpeed
    ldd     EnemyDemo.minY,x
    cmpr    d,w
    ble     @flipYSpeed
    bra     @doneCapYSpeed
@flipYSpeed
    neg     Sprite.speedY,y
@doneCapYSpeed
    stw     Sprite.y,y
    ;
    leax    sizeof{EnemyDemo},x
    leay    sizeof{Sprite},y
    inc     ,s
    lda     ,s
    cmpa    #spriteCount
    lblt    @enemyLoop
    puls    a
    ;
    pulsw
    puls    a,b,x,y,pc

GetSpriteLookDirection
; ```plaintext
; > y = Address of sprite
; < d = Address of compiled sprite
; ```
    tst     Sprite.speedX,y
    bgt     @positive_x
    blt     @negative_x
    tst     Sprite.speedY,y
    bgt     @positive_y
    blt     @negative_y
    ldd     #Hero.Idle
    bra     @done
@positive_x
    tst     Sprite.speedY,y
    bgt     @positive_x_positive_y
    blt     @positive_x_negative_y
    ldd     #Hero.Right
    bra     @done
@negative_x
    tst     Sprite.speedY,y
    bgt     @negative_x_positive_y
    blt     @negative_x_negative_y
    ldd     #Hero.Left
    bra     @done
@positive_y
    ldd     #Hero.Down
    bra     @done
@negative_y
    ldd     #Hero.Up
    bra     @done
@positive_x_positive_y
    ldd     #Hero.DownRight
    bra     @done
@positive_x_negative_y
    ldd     #Hero.UpRight
    bra     @done
@negative_x_positive_y
    ldd     #Hero.DownLeft
    bra     @done
@negative_x_negative_y
    ldd     #Hero.UpLeft
@done
    rts

SaveOffScreenSpriteBackground
; ```plaintext
; > a = Sprite index
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldx     #spriteAddresses
    lsla
    ldx     a,x
    ;
    lda     Sprite.isActive,x
    lbeq    @doneWithSprite
    ;
    ldd     Sprite.x,x
    addd    #32
    cmpd    <playfieldTopLeftX
    lble    @doneWithSprite
    subd    #(320+32)
    cmpd    <playfieldTopLeftX
    lbgt    @doneWithSprite
    ;
    ldd     Sprite.y,x
    addd    #32
    cmpd    <playfieldTopLeftY
    lble    @doneWithSprite
    subd    #(143+32)
    cmpd    <playfieldTopLeftY
    lbgt    @doneWithSprite
    ;
    ldy     Sprite.offScreenBackgroundAddress,x
    oim     1;SpriteBackground.isCaptured,y
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    ldb     #LOGICAL_8000_9FFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    inca
    incb
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    ldd     SpriteBackground.byteOffset,y
    leay    SpriteBackground.buffer,y
    ldx     #$8000
    leax    d,x
    ;
* #region Blit buffer to background
    ldw     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    ;
* #endregion
    ;
    ldb     #LOGICAL_A000_BFFF
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    decb
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
@doneWithSprite
    ;
    pulsw
    puls    a,b,x,y,pc

SaveOffScreenSpriteBackgrounds
    pshs    a
    ;
    clra
@spriteLoop
    lbsr    SaveOffScreenSpriteBackground
    inca
    cmpa    #spriteCount
    blt     @spriteLoop
    ;
    puls    a,pc

RestoreOffScreenSpriteBackground
; ```plaintext
; > a = Sprite index
; ```
;***********************
    ; If sprite.isActive && sprite.background.isCaptured
    ;     Map $8000-$9FFF to sprite.background.firstOfTwoPhysicalBlocks
    ;     Map $A000-$BFFF to sprite.background.firstOfTwoPhysicalBlocks + 1
    ;     Restore sprite's background starting at $8000 + sprite.background.byteOffset
    ; End If
    pshs    y,x,b,a
    pshsw
    ;
    ldx     #spriteAddresses
    lsla
    ldx     a,x
    ;
    lda     Sprite.isActive,x
    lbeq    @doneWithSprite
    ldy     Sprite.offScreenBackgroundAddress,x
    lda     SpriteBackground.isCaptured,y
    lbeq    @doneWithSprite
    clr     SpriteBackground.isCaptured,y
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    ldb     #LOGICAL_8000_9FFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    inca
    incb
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    ldd     SpriteBackground.byteOffset,y
    leay    SpriteBackground.buffer,y
    ldx     #$8000
    leax    d,x
    ;
* #region Blit background to buffer
    ;
    ldw     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    ;
* #endregion
    ;
    ldb     #LOGICAL_A000_BFFF
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    decb
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
@doneWithSprite
    ;
    pulsw
    puls    a,b,x,y,pc

RestoreOffScreenSpriteBackgrounds
    pshs    a
    ;
    clra
@spriteLoop
    lbsr    RestoreOffScreenSpriteBackground
    inca
    cmpa    #spriteCount
    blt     @spriteLoop
    ;
    puls    a,pc

ScreenBuffer        STRUCT
verticalOffset                      rmb 2
horizontalOffset                    rmb 1
firstPhysicalBlockOfBuffer          rmb 1
firstPhysicalBlockOfVerticalStripe  rmb 1
leftPlayfieldXInSlice               rmb 2
videoBank                           rmb 1
                    ENDSTRUCT

screenBuffers                       rmb sizeof{ScreenBuffer}*2
onScreenBuffer                      fdb screenBuffers
offScreenBuffer                     fdb screenBuffers+sizeof{ScreenBuffer}

UpdateScroll
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldw     offScreenBuffer,pcr
    ldb     ScreenBuffer.firstPhysicalBlockOfBuffer,w
    ;
    clra
    tfr     d,y
    clr     ScreenBuffer.leftPlayfieldXInSlice,w
    clr     ScreenBuffer.leftPlayfieldXInSlice+1,w
    ldd     <playfieldTopLeftX
    andb    #%11111100              ; We can only scroll horizontally to multiples of 4 x-coordinates, so zero out bits 0 and 1.
@checkForFirstSlice
    cmpd    #192                    ; Are we in slice 1? 192 = first x-coordinate of slice 2
    bge     @checkForSecondSlice    ; Nope, check if we're in the next slice
    ldx     #$0000                  ; We're in slice 1.  0 <= d <= 191.  d ÷ 2 = GIME Horizontal Offset.  $0000 = GIME Vertical Offset.
    bra     @updateScreen
@checkForSecondSlice
    cmpd    #384                    ; Are we in slice 2? 384 = first x-coordinate of slice 3
    bge     @checkForThirdSlice     ; Nope, check if we're in the next slice
    subd    #192                    ; We're in slice 2.  192 <= d <= 383.  (d-192) ÷ 2 = GIME Horizontal Offset.  $4000 = GIME Vertical Offset.
    ldx     #192
    stx     ScreenBuffer.leftPlayfieldXInSlice,w
    ldx     #$4000
    leay    $10,y
    bra     @updateScreen
@checkForThirdSlice
    cmpd    #512                    ; Are we in slice 3? 512 = first x-coordinate of slice 4
    bge     @fourthSlice            ; Nope, we're in slice 4
    subd    #384                    ; We're in slice 3.  384 <= d <= 511.  (d-384) ÷ 2 = GIME Horizontal Offset.  $8000 = GIME Vertical Offset.
    ldx     #384
    stx     ScreenBuffer.leftPlayfieldXInSlice,w
    ldx     #$8000
    leay    $20,y
    bra     @updateScreen
@fourthSlice
    subd    #512                    ; We're in slice 4.  512 <= d <= 704.  (d-512) ÷ 2 = GIME Horizontal Offset. $C000 = GIME Vertical Offset.
    ldx     #512
    stx     ScreenBuffer.leftPlayfieldXInSlice,w
    ldx     #$C000
    leay    $30,y
    ;
@updateScreen                       ; b = Horizontal Offset * 2, X = Vertical Offset for top of slice, @videoBank = Video Bank Select, y = first physical block of slice.
    exg     y,d
    stb     ScreenBuffer.firstPhysicalBlockOfVerticalStripe,w
    exg     y,d
    lsrb
    lsrb                            ; b = # pixels from left of vertical slice ÷ by 4 (horizontal offset is multiple of 2 bytes)
    stb     ScreenBuffer.horizontalOffset,w
    ldd     <playfieldTopLeftY      ; GIME Vertical Offset adjusts the start address of video in 8-byte increments.  256 ÷ 8 = 32.  We need to add 32 to Vertical Offset for each Playfield Y.
    lsld
    lsld
    lsld
    lsld
    lsld
    leax    d,x
    stx     ScreenBuffer.verticalOffset,w
    ;
    pulsw
    puls    a,b,x,y,pc

UpdateSprites
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    clra
    pshs    a
@nextSprite
    ;
    ldw     offScreenBuffer,pcr
    ldy     #spriteAddresses
    lsla
    ldy     a,y
    ldx     Sprite.offScreenBackgroundAddress,y
    ;
    lda     Sprite.isActive,y
    lbeq    @doneWithSprite
    ;
    ; Calculate first and second physical blocks and map them to $8000-$9FFF and $A000-$BFFF
    ldd     Sprite.y,y
    tst     ,s
    bne     @doneAdjustHeroForJumpHeight.1
    subd    currentJumpHeight,pcr
    subd    currentJumpHeight,pcr
@doneAdjustHeroForJumpHeight.1
    lsld
    lsld
    lsld                                        ; a = sprite.y ÷ 32
    adda    ScreenBuffer.firstPhysicalBlockOfVerticalStripe,w   ; a = firstPhysicalBlockOfVerticalStripe + (sprite.y ÷ 32)
    sta     SpriteBackground.firstOfTwoPhysicalBlocks,x
    ;
    ; Calculate sprite's byte offset from top of first block based on sprite.x, sprite.y
    ; (sprite.y & $001F) * 256 + (sprite.x - leftPlayfieldXInSlice) ÷ 2
    ldd     Sprite.x,y
    tst     ,s
    bne     @doneAdjustHeroForJumpHeight.2
    subd    currentJumpHeight,pcr
@doneAdjustHeroForJumpHeight.2
    subd    ScreenBuffer.leftPlayfieldXInSlice,w
    lsrd
    pshs    b
    ldd     Sprite.y,y
    tst     1,s
    bne     @doneAdjustHeroForJumpHeight.3
    subd    currentJumpHeight,pcr
    subd    currentJumpHeight,pcr
@doneAdjustHeroForJumpHeight.3
    tfr     b,a
    puls    b
    anda    #$1F
    std     SpriteBackground.byteOffset,x
    ;
    lbsr    GetSpriteLookDirection
    lde     #HERO_NORMAL_DATA_PHYSICAL_BLOCK
    std     Sprite.compiled,y
    ste     Sprite.compiledPhysicalBlock,y
    ;
@doneWithSprite
    inc     ,s
    lda     ,s
    cmpa    #spriteCount
    lblt    @nextSprite
    puls    a
    ;
    pulsw
    puls    a,b,x,y,pc

DrawOffScreenSprite
; ```plaintext
; > a = Sprite index
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldx     #spriteAddresses
    lsla
    ldx     a,x
    ldy     Sprite.offScreenBackgroundAddress,x
    ldw     offScreenBuffer,pcr
    ;
    lda     Sprite.isActive,x
    lbeq    @doneWithSprite
    ;
    ldd     Sprite.x,x
    addd    #32
    cmpd    <playfieldTopLeftX
    lble    @doneWithSprite
    subd    #(320+32)
    cmpd    <playfieldTopLeftX
    lbgt    @doneWithSprite
    ;
    ldd     Sprite.y,x
    addd    #32
    cmpd    <playfieldTopLeftY
    lble    @doneWithSprite
    subd    #(143+32)
    cmpd    <playfieldTopLeftY
    lbgt    @doneWithSprite
    ;
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    ldb     #LOGICAL_8000_9FFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    inca
    incb
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     Sprite.compiledPhysicalBlock,x
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ;
    ldd     SpriteBackground.byteOffset,y
    tfr     d,u
    leau    $8000,u
    jsr     [Sprite.compiled,x]
    ;
    puls    a
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ldb     #LOGICAL_A000_BFFF
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    decb
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
@doneWithSprite
    ;
    pulsw
    puls    a,b,x,y,pc

DrawOffScreenSprites
    pshs    a
    ;
    clra
@spriteLoop
    lbsr    DrawOffScreenSprite
    inca
    cmpa    #spriteCount
    blt     @spriteLoop
    ;
    puls    a,pc

UpdateScore
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    lde     #$C0                                ; First block in 4th bank of 512K
    ldd     <playfieldTopLeftX
    cmpd    #192
    blt     @gotStartBlock
    adde    #16
    cmpd    #384
    blt     @gotStartBlock
    adde    #16
    cmpd    #512
    blt     @gotStartBlock
    adde    #16
@gotStartBlock
    ;
    ldd     <playfieldTopLeftY
    addd    #143                                ; # lines down from current Y where score starts
    ;
    lsrd                                        ; ÷ 2 (shifts bit 0 of a into bit 7 of b)
    lsrb                                        ; ÷ 4
    lsrb                                        ; ÷ 8
    lsrb                                        ; ÷ 16
    lsrb                                        ; b = low 9 bits of (playfieldTopLeftY + 143) ÷ 32 (# of 8K blocks (8k ÷ 512 bytes per line = 16) our score starts in)
    addr    e,b
    tfr     b,a                                 ; a = Physical block where our Y starts
    ;
    ldb     #LOGICAL_A000_BFFF
    tfr     a,e
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    tfr     e,a
    ;
    inca
    bne     @continue
    lda     #$C0
@continue
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ;
    ldd     <playfieldTopLeftY
    addd    #143
    andb    #$1F
    tfr     b,a
    clrb
    ldx     #$A000
    leax    d,x
    leay    scoreTemplate,pcr
    ldw     #80*7
    tfm     y+,x+
    ;
    leax    -80*7,x
    bsr     PrintBcdValues
    ;
    puls    a
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    puls    a
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,y,pc

PrintBcdValues
; ```plainttext
; > x = logical address of first byte of text score area
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ; Print Timer
    leax    80*1+17*2,x
    lda     bcdTimer,pcr
    tfr     a,b
    lsra
    lsra
    lsra
    lsra
    andb    #$0F
    addd    #$3030
    sta     ,x++
    stb     ,x
    ;
    ; Print Gems
    leax    80*2+-9*2,x
    lda     bcdGems,pcr
    tfr     a,b
    lsra
    lsra
    lsra
    lsra
    andb    #$0F
    addd    #$3030
    sta     ,x++
    stb     ,x
    ;
    ; Print Score
    leax    10*2,x
    leay    bcdScore,pcr
    lda     ,y+
    anda    #$0F
    adda    #$30
    sta     ,x++
    lde     #3
@loop
    lda     ,y+
    tfr     a,b
    lsra
    lsra
    lsra
    lsra
    andb    #$0F
    addd    #$3030
    sta     ,x++
    stb     ,x++
    dece
    bne     @loop
    ;
    ; Print Keys
    leax    7*2,x
    lda     bcdKeys,pcr
    tfr     a,b
    lsra
    lsra
    lsra
    lsra
    andb    #$0F
    addd    #$3030
    sta     ,x++
    stb     ,x
    ;
    pulsw
    puls    a,b,x,y,pc

AddToBcdScore
; ```plaintext
; < a = BCD number to add to score (0-99)
; ```
;***********************
    pshs    x,a
    ;
    leax    bcdScore+3,pcr
    lda     ,x
    adda    ,s
    daa
    sta     ,x
    lda     ,-x
    adca    #0
    daa
    sta     ,x
    lda     ,-x
    adca    #0
    daa
    sta     ,x
    lda     ,-x
    adca    #0
    daa
    sta     ,x
    ;
    puls    a,x,pc

SubtractFromBcdScore
; ```plaintext
; < a = BCD number to subtract from score (1-99; Cannot subtract 0)
; ```
;***********************
    pshs    x,a
    ;
    lda     #$99
    suba    ,s
    inca
    daa
    pshs    a
    ;
    leax    bcdScore+3,pcr
    lda     ,x
    adda    ,s+
    daa
    sta     ,x
    lda     ,-x
    adca    #$99
    daa
    sta     ,x
    lda     ,-x
    adca    #$99
    daa
    sta     ,x
    lda     ,-x
    adca    #$99
    daa
    sta     ,x
    ;
    puls    a,x,pc

AddToSingleByteBcd
; ```plaintext
; < x = Address of BCD number to add to
; < a = BCD number to add (0-99)
; ```
;***********************
    pshs    a
    ;
    lda     ,x
    adda    ,s
    daa
    sta     ,x
    ;
    puls    a,pc

SubtractFromSingleByteBcd
; ```plaintext
; < x = Address of BCD number to subtract from
; < a = BCD number to subtract (1-99; Cannot subtract 0)
; ```
;***********************
    pshs    a
    ;
    andcc   #%11011111
    lda     #$99
    suba    ,s
    inca
    daa
    pshs    a
    ;
    lda     ,x
    adda    ,s+
    daa
    sta     ,x
    ;
    puls    a,pc

SwapVideoBuffers
    pshs    y,x,b,a
    ;
    clra
    pshs    a
@nextSprite
    ldx     #spriteAddresses
    lsla
    ldx     a,x
    ldd     Sprite.onScreenBackgroundAddress,x
    ldy     Sprite.offScreenBackgroundAddress,x
    std     Sprite.offScreenBackgroundAddress,x
    sty     Sprite.onScreenBackgroundAddress,x
    inc     ,s
    lda     ,s
    cmpa    #spriteCount
    blt     @nextSprite
    puls    a
    ;
    ldx     onScreenBuffer,pcr
    ldy     offScreenBuffer,pcr
    stx     offScreenBuffer,pcr
    sty     onScreenBuffer,pcr
    ;
    lda     ScreenBuffer.videoBank,y
    sta     GIME_VideoBankSelect_FF9B
    ldd     ScreenBuffer.verticalOffset,y
    std     GIME_VerticalOffset1_FF9D
    ;         ┌────────── Bit  0   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%10000001
    sta     GIME_VideoMode_FF98
    ;         ┌────────── Bit  0   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%00111110
    sta     GIME_VideoResolution_FF99
    ldb     ScreenBuffer.horizontalOffset,y
    orb     #%10000000;             ; Keep HVEN (bit 7) of GIME Horizontal Offset set to keep us in 256-byte wide mode.
    stb     GIME_HorizontalOffset_FF9F
    ;
    puls    a,b,x,y,pc

DisplayLevelLoadingScreen
;***********************
    pshs    x,a
    ;
    lbsr    RestorePalette
    lbsr    Set80x24TextMode
    leax    @loadingLevel,pcr
    lbsr    PrintString
    lda     currentLevel
    lbsr    PrintHexByte
    ;
    puls    a,x,pc
@loadingLevel                   fcn "Loading level $"

ProcessGameKeys
;***********************
    lbsr    ClearGameKeys
    leax    keyColumnScans,pcr
    ldb     #1
@checkUpArrow
    lda     3,x
    anda    #%00001000
    beq     @checkDownArrow
    stb     shouldAccelerateUp,pcr
@checkDownArrow
    lda     4,x
    anda    #%00001000
    beq     @checkLeftArrow
    stb     shouldAccelerateDown,pcr
@checkLeftArrow
    lda     5,x
    anda    #%00001000
    beq     @checkRightArrow
    stb     shouldAccelerateLeft,pcr
@checkRightArrow
    lda     6,x
    anda    #%00001000
    beq     @checkQ
    stb     shouldAccelerateRight,pcr
@checkQ
    lda     1,x
    anda    #%00000100
    beq     @checkSpace
    stb     shouldQuit,pcr
@checkSpace
    lda     7,x
    anda    #%00001000
    beq     @checkN
    stb     shouldJump,pcr
@checkN
    lda     6,x
    anda    #%00000010
    beq     @checkP
    stb     shouldCycleToNextLevel,pcr
@checkP
    lda     0,x
    anda    #%00000100
    beq     @checkLT
    stb     shouldCycleToPreviousLevel,pcr
* @checkW
*     lda     7,x
*     anda    #%00000100
*     beq     @checkLT
*     stb     shouldCycleToNextWaveTable,pcr
@checkLT
    lda     4,x
    anda    #%00100000
    beq     @checkGT
    cmpa    12,x
    beq     @checkGT
    stb     shouldDebugLT,pcr
@checkGT
    lda     6,x
    anda    #%00100000
    beq     @doneCheckingKeys
    cmpa    14,x
    beq     @doneCheckingKeys
    stb     shouldDebugGT,pcr
@doneCheckingKeys
    ldd     ,x
    std     8,x
    ldd     2,x
    std     10,x
    ldd     4,x
    std     12,x
    ldd     6,x
    std     14,x
    rts

ClearGameKeys
;***********************
    clr     shouldAccelerateUp,pcr
    clr     shouldAccelerateDown,pcr
    clr     shouldAccelerateLeft,pcr
    clr     shouldAccelerateRight,pcr
    clr     shouldQuit,pcr
    clr     shouldJump,pcr
    clr     shouldCycleToNextLevel,pcr
    clr     shouldCycleToPreviousLevel,pcr
    * clr     shouldCycleToNextWaveTable,pcr
    clr     shouldDebugLT,pcr
    clr     shouldDebugGT,pcr
    rts

SetEgaPalette:
;***********************
; ```plaintext
; > currentLevel = level
; ```
    pshs    y,x,b,a
    ;
    ; Store the current pallete values for later restoring
    ldy     #ColorPaletteFirstRegister_FFB0
    leax    paletteStore,pcr
    ldb     #15
@loop_store
    lda     b,y
    sta     b,x
    decb
    bpl     @loop_store
@done_store
    ;
    ; Set the pallete
    leax    palettes,pcr
    clra
    ldb     currentLevel,pcr
    lsld
    lsld
    lsld
    lsld                        ; d = level * 16
    leax    d,x                 ; index into palettes for level's palette
    ldb     #15
@loop_set
    lda     b,x
    sta     b,y
    decb
    bpl     @loop_set
@done_set
    ;
    puls    a,b,x,y,pc

SavePalette:
;***********************
    pshs    y,x
    pshsw
    ;
    ldy     #ColorPaletteFirstRegister_FFB0
    leax    paletteStore,pcr
    ldw     #15
    tfm     y+,x+
    ;
    pulsw
    puls    x,y,pc

RestorePalette:
;***********************
    pshs    y,x
    pshsw
    ;
    ldy     #ColorPaletteFirstRegister_FFB0
    leax    paletteStore,pcr
    ldw     #15
    tfm     x+,y+
    ;
    pulsw
    puls    x,y,pc

PlayTune
    ; ```plaintext
    ; > a = Index of tune to play
    ; ```
    pshs    x,b,a
    ;
    tst     tunes.tonesRemaining,pcr
    bne     @done
    ;
    leax    tunes.lengths,pcr
    ldb     a,x
    stb     tunes.tonesRemaining,pcr
    lsla
    leax    tunes.addresses,pcr
    ldx     a,x                             ; x = address of first tone in tune
    stx     tunes.toneInTuneAddress,pcr
    lda     Tone.duration,x
    sta     tunes.durationRemaining,pcr
    ldd     Tone.frequencyValue,x
    std     freqValue+1,pcr
    ;
@done
    puls    a,b,x,pc

ProgressTune
    tst     tunes.tonesRemaining,pcr
    beq     @done
    dec     tunes.durationRemaining,pcr
    bne     @done
    ;
    ; Increment toneInTuneOffset to the next tone in the tune
    ldx     tunes.toneInTuneAddress,pcr
    leax    sizeof{Tone},x
    stx     tunes.toneInTuneAddress,pcr
    ; Decrement tonesRemaining
    dec     tunes.tonesRemaining,pcr
    beq     @done
    ; Set next tone's duration
    lda     Tone.duration,x
    sta     tunes.durationRemaining,pcr
    ; Set next tone's frequency
    ldd     Tone.frequencyValue,x
    std     freqValue+1,pcr
    ;
@done
    rts

FIRQHandler
    std     @restore+1
    ;
    andcc   #%11101111                          ; unmask IRQ so we don't delay vsync.  Vsync needs to restart the timer precisely on time.
    inc     <firqCount
    lda     <firqCount
    cmpa    #70
    bne     @handleSound
    tst     <splitScreen
    beq     @handleSound
*     lda     #3
* @loop
*     deca
*     bne     @loop
    ;
    ;         Video Mode
    ;         ┌────────── Bit  0   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%00000011
    sta     GIME_VideoMode_FF98
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (Graphics: 000=16,001=20,010=32,011=40,100=64,101=80,110=128,111=160 bytes per row, Text: 0x0=32,0x1=40,1x0=64,1x1=80)
    ;         ││││││┌┬─── Bits 1-0 - CRES (Graphics: 00=2, 01=4, 10=16, 11=undefined colors, Text: x0=No color attributes, x1=Color attributes)
    lda     #%00000101
    sta     GIME_VideoResolution_FF99
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    lda     #%00000011
    sta     GIME_VideoBankSelect_FF9B
    ;
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen Enable
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%00000000
    sta     GIME_HorizontalOffset_FF9F
    ;
@handleSound
            tst     tunes.tonesRemaining
            beq     @done
@wavePtr    ldd     #$0000                      ; Continuously updated by next line after freqValue
freqValue   addd    #$0000                      ; Filled in by PlayTune and ProgressTune
            std     @wavePtr+1,pcr
            sta     @ldWaveTbl+2,pcr
@ldWaveTbl  lda     >waveTable
            sta     PIA1SideADataRegister_FF20
    ;
@done
    lda     GIME_FastInterruptReqEnable_FF93    ; Acknowledge the FIRQ interrupt
@restore    ldd     #$0000
    rti

waitingForVsync     fcb     $00

IRQHandler
;***********************
    clr     <firqCount
highSpeedTimerCount
    ldd     #000        ; Filled in by SetHsyncCountForGimeVersion
    stb     GIME_TimerLSB_FF95
    sta     GIME_TimerMSB_FF94
    ;
    tst     <splitScreen
    beq     @continue
    ;
    ;         Video Mode
    ;         ┌────────── Bit  0   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%10000001
    sta     GIME_VideoMode_FF98
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (Graphics: 000=16,001=20,010=32,011=40,100=64,101=80,110=128,111=160 bytes per row, Text: 0x0=32,0x1=40,1x0=64,1x1=80)
    ;         ││││││┌┬─── Bits 1-0 - CRES (Graphics: 00=2, 01=4, 10=16, 11=undefined colors, Text: x0=No color attributes, x1=Color attributes)
    lda     #%00111110
    sta     GIME_VideoResolution_FF99
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    ldx     onScreenBuffer,pcr
    lda     ScreenBuffer.videoBank,x
    sta     GIME_VideoBankSelect_FF9B
    ;
    lda     ScreenBuffer.horizontalOffset,x
    ora     #%10000000;     ; Keep HVEN (bit 7) of GIME Horizontal Offset set to keep us in 256-byte wide mode.
    sta     GIME_HorizontalOffset_FF9F
    ;
@continue
    leax    keyColumnScans,pcr
    lda     #%11111111
    sta     PIA0SideBDataRegister_FF02  ; Disable all keyboard column strobes
    ;
    ldb     #%00000001      ; Start off reading column 0
@loop_keyscan
    comb
    stb     PIA0SideBDataRegister_FF02  ; Enable the next column
    lda     PIA0SideADataRegister_FF00  ; Read the column
    coma                    ; Invert bits so 1=key down, 0=key up
    sta     ,x+             ; Store in next keyColumnScans element
    ;
    comb
    lslb                    ; Next column. Shift bits in b to the left
    bne     @loop_keyscan
    ;
    inc     <vsyncCount
    clr     waitingForVsync,pcr
    ;
@done
    lda     GIME_InterruptReqEnable_FF92
    rti

WaitForKeyPressNoRom:
;***********************
    pshs    b,a
@waitForNoKey
    bsr     IsKeyDownNoRom
    bne     @waitForNoKey
@waitForKey
    bsr     IsKeyDownNoRom
    beq     @waitForKey
@waitForNoKeyAgain
    bsr     IsKeyDownNoRom
    bne     @waitForNoKeyAgain
    ;
    puls    a,b,pc

IsKeyDownRom:
; ```plaintext
; < a = 1 if any key is down, 0 otherwise
; ```
    ; Make ROM call to see if key is pressed
    jsr     [$A000]
    cmpa    #0
    bhi     @keyPressed
    clra                            ; Return 0 for no key down
    bra     @done
@keyPressed
    lda     #1                      ; Return 1 for key down
@done
    rts

IsKeyDownNoRom:
; ```plaintext
; < a = 1 if any key is down, 0 otherwise
; ```
;***********************
    pshs    x,b,a
    ;
    leax    keyColumnScans,pcr
    lda     #%11111111
    sta     PIA0SideBDataRegister_FF02  ; Disable all keyboard column strobes
    ;
    ldb     #%00000001      ; Start off reading column 0
@loop_keyscan
    comb
    stb     PIA0SideBDataRegister_FF02  ; Enable the next column
    lda     PIA0SideADataRegister_FF00  ; Read the column
    coma                    ; Invert bits so 1=key down, 0=key up
    sta     ,x+             ; Store in next keyColumnScans element
    ;
    comb
    lslb                    ; Next column. Shift bits in b to the left
    bne     @loop_keyscan
    ;
    leax    keyColumnScans,pcr
    lda     #7
@checkScan
    tst     a,x
    bne     @keyDown
    tsta
    beq     @noKeyDown
    deca
    bra     @checkScan
@noKeyDown
    lda     #0                      ; Return 0 for no key down
    bra     @done
@keyDown
    lda     #1                      ; Return 1 for key down
@done
    puls    a,b,x,pc

WaitForVsync:
;***********************
    inc     waitingForVsync,pcr
@wait
    tst     waitingForVsync,pcr
    bne     @wait
    rts

SetFastSpeed:
;***********************
    clr     SetClockSpeedR1_FFD9
    rts

SetSlowSpeed:
;***********************
    clr     ClearClockSpeedR1_FFD8
    rts

ResetRgbPalette:
;***********************
    pshs    y,x,a
    ; Fix an error in SECB where it only set 15 of the 16 palette registers to the RGB colors
    lda     #16
    sta     $E649
    ; Call the SECB routine to set the paleete to the RGB colors
    jsr     $E5FA
    puls    a,x,y,pc

originalVectors    rmb 18

SetupInterruptVectors:
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldx     #$FEEE
    leay    originalVectors,pcr
    ldw     #18
    tfm     x+,y+
    ;
    lda     #$3B             ; rti
    pshs    a
    ldx     #$FEEE
    ldw     #18
    tfm     s,x+
    puls    a
    ;
    * tst     GIME_InterruptReqEnable_FF92
    * tst     GIME_FastInterruptReqEnable_FF93
    ldx     #$FEF4
    lda     #$7E            ; jmp
    sta     ,x+
    leay     FIRQHandler,pcr
    sty     ,x++
    lda     #$7E            ; jmp
    sta     ,x+
    leay    IRQHandler,pcr
    sty     ,x
    ;
    ldd     highSpeedTimerCount+1,pcr
    stb     GIME_TimerLSB_FF95
    sta     GIME_TimerMSB_FF94
    ;
    ;         Fast Interrupt Request Enable Registers - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer FIRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync FIRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync FIRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data FIRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard FIRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge FIRQ, 0 = disable)
    lda     #%00100000
    sta     GIME_FastInterruptReqEnable_FF93    ; Enable Timer FIRQ
    ;
    pulsw
    puls    a,b,x,y,pc

RestoreInterruptVectors:
;***********************
    pshs    y,x
    pshsw
    ;
    ;         Fast Interrupt Request Enable Registers - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer FIRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync FIRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync FIRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data FIRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard FIRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge FIRQ, 0 = disable)
    lda     #%00000000
    sta     GIME_FastInterruptReqEnable_FF93    ; Disable Timer FIRQ

    leax    originalVectors,pcr
    ldy     #$FEEE
    ldw     #18
    tfm     x+,y+
    ;
    pulsw
    puls    x,y,pc

InitializeLevel
; ```plaintext
; > currentLevel = Level #
; ```
    pshs    u,y,x,b,a
    pshsw
    ;
    lda     #6
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock  ; Map the Tile Data block containing the destination tiles
    lda     #POWERUPS_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock  ; Map the Tile Data block containing the destination tiles
    lda     currentLevel,pcr
    leax    levelToFloorTileLookup,pcr
    lda     a,x                             ; a = Tile # for floor tile for this map
    pshs    a
    ; Calculate the physical block the tile is in by taking the high nibble (4 bits)
    ; of the tile #. ex: Tile # $6B is in physical block $06
    lsra
    lsra
    lsra
    lsra
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    puls    a
    ; Calculate the offset into the tile #'s block where tile # starts by taking the
    ; low nibble of the tile # and multiplying by 512 (number of bytes in each tile).
    ; ex: Tile # $6B is at offset $B * 512, or $1600
    anda    #$0F
    lsla
    clrb                                    ; d now contains the low nibble of the tile # * 512
    ldx     #$6000
    leax    d,x                             ; x = address of 512 bytes for the floor tile to copy
    pshs    x
    ldy     #$A000+(512*4)                  ; y = address of first (of 8) 512 bytes to copy the floor tile to
    lda     #8
@nextTile
    ldw     #512
    tfr     y,u
    tfm     x+,y+
    cmpa    #1
    beq     @skipPowerUp                    ; Only draw the 7 powerups on the first 7 floor tiles.  Leave the last floor tile just floor.
    pshs    y,x,a
    nega
    adda    #8                              ; a = powerup index (0-6)
    lsla                                    ; a*2 = index into powerUpCompiledSprites to get the compiled sprite address
    leax    powerUpCompiledSprites,pcr
    jsr     [a,x]                           ; Call compiled sprite to draw the powerup on the floor tile we just copied
    puls    a,x,y
@skipPowerUp
    ldx     ,s
    deca
    bne     @nextTile
    puls    x 
    ;
    ; Copy the current 512 byte map from the loaded map data (2 blocks at #MAP_DATA_PHYSICAL_BLOCK) to currentMap.
    lda     #MAP_DATA_PHYSICAL_BLOCK
    ldb     currentLevel,pcr
    cmpb    #16
    blt     @doneAdjustBlock
    inca
@doneAdjustBlock
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    lda     currentLevel,pcr
    anda    #$0F
    clrb
    lsla                                    ; d = offset into block where 512 byte map data is for currentLevel
    ldx     #$6000
    leax    d,x
    leay    currentMap,pcr
    ldw     #512
    tfm     x+,y+
    ;
@intializeHero
    clr     currentJumpIndex,pcr
    clr     currentJumpHeight,pcr
    clr     currentJumpHeight+1,pcr
    leay    sprites,pcr
    ldd     #1
    stb     Sprite.isActive,y
    ldb     #64
    std     Sprite.x,y
    std     Sprite.y,y
    clr     Sprite.speedY,y
    clr     Sprite.speedX,y
    leax    Sprite.background1,y
    stx     Sprite.onScreenBackgroundAddress,y
    clr     SpriteBackground.isCaptured,x
    leax    Sprite.background2,y
    stx     Sprite.offScreenBackgroundAddress,y
    clr     SpriteBackground.isCaptured,x
    ldx     #Hero.Idle
    stx     Sprite.compiled,y
    lda     #HERO_NORMAL_DATA_PHYSICAL_BLOCK
    sta     Sprite.compiledPhysicalBlock,y
    ;
@intializeDemoEnemies
    leax    enemyDemos,pcr
    leay    sprites+sizeof{Sprite},pcr
    clra
    inca
    pshs    a
@enemyLoop
    ldb     #1
    stb     Sprite.isActive,y
    ldd     EnemyDemo.startX,x
    std     Sprite.x,y
    ldd     EnemyDemo.startY,x
    std     Sprite.y,y
    lda     EnemyDemo.startSpeedX,x
    sta     Sprite.speedX,y
    lda     EnemyDemo.startSpeedY,x
    sta     Sprite.speedY,y
    tfr     y,w
    addw    #Sprite.background1
    stw     Sprite.onScreenBackgroundAddress,y
    clr     SpriteBackground.isCaptured,w
    addw    #sizeof{SpriteBackground}
    stw     Sprite.offScreenBackgroundAddress,y
    clr     SpriteBackground.isCaptured,w
    ldw     #Hero.Idle
    stw     Sprite.compiled,y
    lda     #HERO_NORMAL_DATA_PHYSICAL_BLOCK
    sta     Sprite.compiledPhysicalBlock,y
    ;
    leax    sizeof{EnemyDemo},x
    leay    sizeof{Sprite},y
    inc     ,s
    lda     ,s
    cmpa    #spriteCount
    blt     @enemyLoop
    puls    a
    ;
    clr     <vsyncCount
    clr     <firqCount
    ;
    pulsw
    puls    a,b,x,y,u,pc

PrerenderLevel256ByteVerticalSlice
; ```plaintext
; e > Map X coordinate to start at (0-31)
; a > Start Physical block to render to
; b > Non 0 to indicate map data is actual tile indexes
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    stb     @skipMapTileLookup,pcr
    ;
    sta     @block,pcr
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ldx     #$A000
    clrf            ; Map Y to start at
    lda     #16
    sta     @tileCount,pcr
    lda     #'.
    lbsr    PrintChar80x24
@nextTile
    ; Calculate address of map cell and get tile #
    tfr     f,a
    ldb     #32
    mul
    leay    currentMap,pcr
    leay    d,y
    lda     e,y     ; a = Cell #
    tst     @skipMapTileLookup,pcr
    bne     @skip
    leay    mapToTileLookup,pcr
    lda     a,y     ; a = Tile #
@skip
    ;
    lbsr    DrawTileIn256ByteWideBuffer
    leax    16,x
    ince
    dec     @tileCount,pcr
    bne     @nextTile
    sube    #16
    lda     #16
    sta     @tileCount,pcr
    incf
    cmpf    #16                     ; 16 rows
    bhs     @doneTiles
    inc     @block,pcr
    lda     @block,pcr
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ldx     #$A000
    bra     @nextTile
@doneTiles
    ;
    puls    a
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,y,pc
@tileCount          rmb 1
@block              rmb 1
@skipMapTileLookup  rmb 1

DrawTileIn256ByteWideBuffer
; ```plaintext
; > a = Tile #
; > x = Logical address to start drawing
; - Uses $6000-$7FFF to map tile data in
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldb     mmu+LOGICAL_6000_7FFF,pcr
    pshs    b
    ; Calculate the physical block the tile is in by taking the high nibble (4 bits)
    ; of the tile # and adding the first block # of the tile data.
    ; ex: Tile # $6B is in physical block $10 (first block of tile data) + 6, or $16
    pshs    a
    lsra
    lsra
    lsra
    lsra
    adda    #TILE_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    puls    a
    ; Calculate the offset into the tile #'s block where tile # starts by taking the
    ; low nibble of the tile # and multiplying by 512 (number of bytes in each tile).
    ; ex: Tile # $6B is at offset $B * 512, or $1600
    anda    #$0F
    lsla
    clrb                ; d now contains the low nibble of the tile # * 512
    ;
    ldy     #$6000
    leay    d,y
    ldb     #32         ; Draw 32 rows of the tile
@nextRow
    ldw     #16         ; Draw 16 bytes (32 pixels) in this row
    tfm     y+,x+
    leax    256-16,x    ; Adjust x (where we're drawing) down one row and back to the beginning column
    decb
    bne     @nextRow    ; Keep drawing until all 32 rows have been drawn
    ;
    puls    a
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,y,pc

PrenderAllTilesForDebug
    pshs    x,b,a
    pshsw
;
    clra
    pshs    a
    leax    currentMap,pcr
    ldw     #512
    tfm     s,x+
    puls    a
    leax    currentMap,pcr
    lda     #0
    ldb     #16
@continue
    sta     ,x+
    inca
    cmpa    #$78
    beq     @done
    decb
    bne     @continue
    ldb     #16
    leax    16,x
    bra     @continue
@done
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK
    clre
    ldb     #1
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #4
    lbsr    PrerenderLevel256ByteVerticalSlice
    ;
    clre
    adda    #16
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #4
    lbsr    PrerenderLevel256ByteVerticalSlice
;
    pulsw
    puls    a,b,x,pc

SetHsyncCountForGimeVersion
;***********************
    pshs    u,y,x,b,a
    ;
    sta     $FFD9
    ldu     #$0112
    ldb     1,u
@WaitForStartOfNextTick
    cmpb    1,u
    beq     @WaitForStartOfNextTick
    ldd     #$FFC4
    std     ,u
    ldx     #$FF93
    clr     -2,x
    lda     #$20
    sta     ,x
    ldd     #1
    stb     2,x
    sta     1,x
    ldy     #1
    ldb     ,x
@WaitTimerHighByteToCycleBackToZero
    lda     #$20
    bita    ,x
    beq     @SkipCount
    leay    1,y
@SkipCount
    tst     ,u
    bne     @WaitTimerHighByteToCycleBackToZero
    sty     debugData+DebugData.gimeCheckCount
    sta     $FFD8
    ldd     #685
    cmpy    #7085
    ble     @WeGotUsAn86Gime
    ldd     #689
@WeGotUsAn86Gime
    std     highSpeedTimerCount+1,pcr
    std     debugData+DebugData.highSpeedTimerCountStart
    std     debugData+DebugData.highSpeedTimerCountFinal
    stb     $FFD8
    ;
    puls    a,b,x,y,u,pc

GetHerosCurrentMapCellData
; ```plaintext
; < mapXHeroIsOn                  = X coordinate (0-31) of cell in currentMap Hero is currently on
; < mapYHeroIsOn                  = Y coordinate (0-15) of cell in currentMap Hero is currently on
; < addressOfMapCellHeroIsOn      = address of cell in currentMap Hero is currently on
; < actionIndexForMapCellHeroIsOn = Index into heroActionLookup for action routine for map cell Hero is currently on
; < a                             = Index into heroActionLookup for action routine for map cell Hero is currently on
; ```
;***********************
    pshs    x,b
    ldd     sprites+Sprite.x
    addd    #16                 ; Center of sprite horizontally
    lsrd
    lsrd
    lsrd
    lsrd
    lsrd                        ; d = X coordinate (0-31) in currentMap
    std     mapXHeroIsOn
    ;
    ldd     sprites+Sprite.y
    addd    #16                 ; Center of sprite vertically
    tfr     d,x
    lsrd
    lsrd
    lsrd
    lsrd
    lsrd                        ; d = Y coordinate (0-15) in currentMap
    std     mapYHeroIsOn
    ;
    tfr     x,d                 ; d = Hero Y coordinate + 16 (center of sprite vertically)
    andd    #$FFE0              ; d / 32 = Map Y, * 32 = offset into currentMap for row.  d / 32 * 32 = d and #$FFE0
    addd    mapXHeroIsOn,pcr    ; d = offset into currentMap of cell Hero's center is on
    addd    #currentMap
    std     addressOfMapCellHeroIsOn,pcr
    ;
    lda     [addressOfMapCellHeroIsOn,pcr]
    leax    mapDataToHeroActionIndexLookup,pcr
    lda     a,x
    lsla
    sta     actionIndexForMapCellHeroIsOn,pcr
    ;
    puls    b,x,pc

actionHeroHitNoAction
    rts

actionHeroHitCrazyBumper
    rts

actionHeroHitDamagingFloor
    rts

actionHeroHitFloorFall
    rts

actionHeroHitIceFloor
    rts

actionHeroHitNoFloorFall
    rts

actionHeroHitPushDown
    rts

actionHeroHitPushLeft
    rts

actionHeroHitPushLeftDown
    rts

actionHeroHitPushLeftUp
    rts

actionHeroHitPushRight
    rts

actionHeroHitPushRightDown
    rts

actionHeroHitPushRightUp
    rts

actionHeroHitPushUp
    rts

actionHeroHitWall
    rts

actionHeroHitMagnet
    rts

actionHeroHitGemItem
    pshs    a
    ;
    lda     #tunes.hitGemItem.index
    lbsr    PlayTune
    ;
    puls    a,pc

actionHeroHitPaintItem 
    pshs    a
    ;
    lda     #tunes.hitPaintItem.index
    lbsr    PlayTune
    ;
    puls    a,pc

actionHeroHitWingItem
    pshs    a
    ;
    lda     #tunes.hitWingItem.index
    lbsr    PlayTune
    ;
    puls    a,pc
    rts

actionHeroHitKeyItem
    pshs    a
    ;
    lda     #tunes.hitKeyItem.index
    lbsr    PlayTune
    ;
    puls    a,pc

actionHeroHitClockItem
    pshs    a
    ;
    lda     #tunes.hitClockItem.index
    lbsr    PlayTune
    ;
    puls    a,pc

actionHeroHitSodaItem
    pshs    a
    ;
    lda     #tunes.hitSodaItem.index
    lbsr    PlayTune
    ;
    puls    a,pc

actionHeroHitHammerItem
    pshs    a
    ;
    lda     #tunes.hitHammerItem.index
    lbsr    PlayTune
    ;
    puls    a,pc

    org     $6000-sizeof{Sprite}*6
sprites                     rmb sizeof{Sprite}*6

    END     start
