openFileForInput                rmb     2
openFileForOutput               rmb     2
originalErrorHandler            rmb     2
originalErrorHandlerOpCode      rmb     1
diskIrq                         rmb     2
errorHandlerStack               rmb     2
bytesToRead                     rmb     2
bytesReadBuffer                 rmb     2
physicalBlock                   rmb     1
physicalOffset                  rmb     2
progressCallback                fdb     $0000
addressOfFilename               rmb     2

noDiskSupport                  fcn "Disk support not detected\r"

;***********************
InitializeDiskIo
; ```plaintext
; < cc:cf = set on error, clear on success`
; ```plaintext
;***********************
    pshs    u,y,x
    ldx     $C004               ; Get DSKCON address pointer
    cmpx    #$D75F              ; Is it Disk Basic 1.1?
    beq     @disk_11
    cmpx    #$d66c              ; Is it Disk Basic 1.0?
    beq     @disk_10
    ldx     #0
    ldu     #0
    ldy     #0
    bra     @set_routines
@disk_10
    ldx     #$C956              ; address of "open sequential file for output" routine
    ldu     #$C959              ; address of "open sequential file for input" routine
    ldy     #$D7C4              ; point to Disk IRQ handler
    bra     @set_routines
@disk_11
    ldx     #$CA04              ; address of "open sequential file for output" routine
    ldu     #$CA07              ; address of "open sequential file for input" routine
    ldy     #$D8B7              ; point to Disk IRQ handler
@set_routines
    stx     openFileForOutput,pcr  ; save address of "open for output"
    stu     openFileForInput,pcr   ; save address of "open for input"
    sty     diskIrq,pcr         ; save IRQ hook for Basic
    bne     @no_error
    coma                        ; set carry to indicate error
    bra     @done
@no_error
    clra                        ; clear carry to indicate success
@done
    puls    x,y,u,pc

;***********************
SaveErrorHandlerStack
;***********************
    puls    y
    sts     errorHandlerStack,pcr
    jmp     ,y

;***********************
SetDiskErrorHandler
; ```plaintext
; > x = error handler
; ```
    pshs    y,a
    lda     ErrorHandlerVector
    ldy     ErrorHandlerVector+1
    sta     originalErrorHandlerOpCode,pcr
    sty     originalErrorHandler,pcr
    lda     #$7E        ; JMP
    sta     ErrorHandlerVector
    stx     ErrorHandlerVector+1
    puls    a,y,pc

;***********************
ClearDiskErrorHandler
;***********************
    pshs    x,a
    lda     originalErrorHandlerOpCode,pcr
    ldx     originalErrorHandler,pcr
    sta     ErrorHandlerVector
    stx     ErrorHandlerVector+1
    puls    a,x,pc

;***********************
FileOpenCloseErrorHandler
;***********************
    ldmd    #1
    lds     errorHandlerStack,pcr   ; Restore stack to when handler was set
    bsr     ClearDiskErrorHandler
    coma                            ; set cc flag to indicate error
    rts                             ; Returns from the function that set the trap

;***********************
FileReadErrorHandler
;***********************
    ldmd    #1
    lds     errorHandlerStack       ; Restore stack to when handler was set
    bsr     ClearDiskErrorHandler
    bsr     CloseAllFiles
    coma                            ; set cc flag to indicate error
    rts                             ; Returns from the function that set the trap

;***********************
CloseAllFiles
; ```plaintext
; << cc = set on error
; ```
    pshs    b
    lbsr    SaveErrorHandlerStack
    leax    FileOpenCloseErrorHandler,pcr
    lbsr    SetDiskErrorHandler
    ldmd    #0
    jsr     $A426
    ldmd    #1
    lbsr    ClearDiskErrorHandler
    clra
    puls    b,pc

;***********************
OpenFileForRead
; ```plaintext
; > x = address of 8.3 filename, \0 terminated.
; ```
    lbsr    SaveErrorHandlerStack
    pshs    u,x,b,a
    ;
    lbsr    CopyFilenameToBuffer
    leax    FileOpenCloseErrorHandler,pcr
    lbsr    SetDiskErrorHandler
    ;
    ldmd    #0
    jsr     [openFileForInput]          ; Open the file
    ldmd    #1
    ;
    lbsr    ClearDiskErrorHandler       ; Clear error handler
    clra
    puls    a,b,x,u,pc

;***********************
ReadByteFromFile
; ```plaintext
; < a  = byte read
; < cc = set on error
; ```
    lbsr    SaveErrorHandlerStack
    pshs    x,b
    leax    FileReadErrorHandler,pcr
    lbsr    SetDiskErrorHandler
    ;
    ldmd    #0
    jsr     ConsoleIn
    ldmd    #1
    ;
    lbsr     ClearDiskErrorHandler
    clrb                                ; Clear cc to indicate no error
    puls    b,x,pc

;***********************
ReadBytesFromFileIntoBuffer
; ```plaintext
; > word bytesToRead     = Number of bytes to read
; > word bytesReadBuffer = Address of buffer to read bytes into
; 
; < x   = number of bytes actually read
; < cc  = set if error
; ```
    lbsr    SaveErrorHandlerStack
    pshs    y,b,a
    leax    FileReadErrorHandler,pcr
    lbsr    SetDiskErrorHandler
    ldy     bytesReadBuffer,pcr
    ldx     #0
@readBytes
    cmpx    bytesToRead,pcr       ; Have we read the request # of bytes?
    beq     @done                 ; If so, done with no error
    ldmd    #0
    jsr     ConsoleIn             ; Read a byte from the file
    ldmd    #1
    tst     ConsoleInBufferFlag   ; Did we hit EOF?
    bne     @done                 ; If so, done with no error
    sta     ,y+                   ; Store byte read into buffer
    leax    1,x                   ; Increment number of bytes read
    bra     @readBytes            ; Loop back for more
@done
    lbsr    ClearDiskErrorHandler
    clra
    puls    a,b,y,pc

;***********************
CopyFilenameToBuffer
; ```plainttext
; > x = address of 8/3 filename.
;       Filename is 11 bytes.
;       First 8 are filename right padded with spaces.
;       Last 3 are extension right padded with spaces.
; ```
    pshs    y,x,b,a
    ; Clear filename buffer
    ldy     #FileNameBuffer
    ldb     #11
@filenameLoop
    lda     ,x+
    sta     ,y+
    decb
    bne     @filenameLoop
    ;
    puls    a,b,x,y,pc

;***********************
@errorMessageStart  fcn '[%Error $'
@errorMessageEnd    fcn ']'
LoadFileStartingAtPhysicalBlock
; ```plaintext
; > addressOfFilename          = Address of 8.3 filename, \0 terminated
; > physicalBlock              = Physical block to start loading at
; > physicalOffset             = Offset into first physical block to start loading at
; > safeMmuLogicalBlock        = Logical block that is safe to use for mapping physical blocks
; > safeMmuLogicalBlockAddress = Starting logical address of safeMmuLogicalBlock
; > progressCallback           = Address of routine to call before each block is read
; 
; < physicalBlock              = Physical block of next byte after last byte read
; < physicalOffset             = Offset into physicalBlock of next byte after last byte read into
; < cc:carry                   = set on error
; ```
;***********************
    pshs    x,b,a
    ; Map initial block
    lda     physicalBlock,pcr
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    pshs    a
    ; Open file
    ldx     addressOfFilename,pcr
    lbsr    OpenFileForRead
    bcs     @error
    ; Calculate logical address and number of bytes to read in first block
    ldd     physicalOffset,pcr
    ldx     safeMmuLogicalBlockAddress,pcr
    leax    d,x
    stx     bytesReadBuffer,pcr
    negd
    ldx     #$2000
    leax    d,x
    stx     bytesToRead,pcr
    ;
@loadNextBlock
    tst     progressCallback,pcr
    beq     @doneCallback
    jsr     [progressCallback]
@doneCallback
    lda     physicalBlock,pcr
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    lbsr    ReadBytesFromFileIntoBuffer
    bcs     @error
    cmpx    bytesToRead,pcr
    blo     @doneLastPartialBlock
    inc     physicalBlock,pcr
    ldd     #$0000
    std     physicalOffset,pcr
    ldd     #$2000
    std     bytesToRead,pcr
    bra     @loadNextBlock
    ;
@doneLastPartialBlock
    ldd     physicalOffset,pcr
    leax    d,x
    stx     physicalOffset,pcr
    lbsr    CloseAllFiles
    puls    a
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    clra                        ; clear cc:carry to indicate success
    bra     @done
@error
    puls    a
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    leax    @errorMessageStart,pcr
    lbsr    PrintString
    tfr     b,a
    lbsr    PrintHexByte
    lda     #']'
    lbsr    PrintChar80x24
    lda     #13
    lbsr    PrintChar80x24
    coma                        ; set cc:carry to indicate error
@done
    puls    a,b,x,pc
