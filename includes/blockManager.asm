mmu                             fcb $38,$39,$3A,$3B,$3C,$3D,$3E,$3F
safeMmuLogicalBlock             fcb LOGICAL_4000_5FFF ; MMU block to use temporarily to access physical blocks
safeMmuLogicalBlockAddress      fdb $4000

;***********************
MapPhysicalBlockToSafeMmuLogicalBlock:
; ```plaintext
; > a = physical block # to map
; < a = originally mapped physical block #
; ```
    pshs    b
    ;
    ldb     safeMmuLogicalBlock,pcr
    bsr     MapPhysicalBlockToLogicalBlock
    ;
    puls    b,pc

;***********************
MapPhysicalBlockToLogicalBlock:
; ```plaintext
; > a = physical block # to map
; > b = logical block # to map
; < a = originally mapped physical block #
; ```
    pshs    x,b
    ; Map the phsyical block in a to the logical block in b
    ldx     #MMU_BLOCK_REGISTERS_FIRST
    abx
    sta     ,x
    ; Get original physical block mapped to safeMmuLogicalBlock
    leax    mmu,pcr
    abx
    tfr     a,b
    lda     ,x
    stb     ,x
    ;
    puls    b,x,pc

;***********************
SetSafeMmuScratchBlock:
; ```plaintext
; > a = Logical MMU block to use as a safe scratch block for mapping
; ```
;***********************
    pshs    x,a,cc
    orcc    #$50
    sta     safeMmuLogicalBlock,pcr
    ldx     #$0000
@loop
    tsta
    beq     @doneMultiply
    leax    $2000,x
    deca
    bra     @loop
@doneMultiply
    stx     safeMmuLogicalBlockAddress,pcr
    ;
    puls    cc,a,x,pc
