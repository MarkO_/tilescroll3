printChar80x24_CurrentCursorX   fcb 0
printChar80x24_CurrentCursorY   fcb 0

;***********************
PrintHexWordWithSpace
; ```plaintext
; > d = number to print as four hex digits
; ```
    pshs    x,a
    lbsr    PrintHexByte
    tfr     b,a
    lbsr    PrintHexByteWithSpace
    puls    a,x,pc
;***********************
PrintHexWord
; ```plaintext
; > d = number to print as four hex digits
; ```
    pshs    x,a
    lbsr    PrintHexByte
    tfr     b,a
    lbsr    PrintHexByte
    puls    a,x,pc

;***********************
@buffer     fcb     0,0,0,0
PrintHexByte
; ```plaintext
; > a = number to print as two hex digits
; ```
    pshs    x,b,a
    leax    @buffer,pcr
    tfr     a,b
    lsra
    lsra
    lsra
    lsra
    andb    #$0F
    addd    #'0*256+'0
    cmpa    #'9
    bls     @doneFirstDigit
    adda    #7
@doneFirstDigit
    cmpb    #'9
    bls     @doneSecondDigit
    addb    #7
@doneSecondDigit
    std     ,x
    ;
    bsr     PrintString
    puls    a,b,x,pc
;***********************
PrintHexByteWithSpace
; ```plaintext
; > a = number to print as two hex digits
; ```
;***********************
    pshs    x,a
    leax    @buffer,pcr
    lda     #32
    sta     2,x
    lda     ,s
    bsr     PrintHexByte
    clr     2,x
    puls    a,x,pc

;***********************
PrintString
; ```plaintext
; > x = pointer to string to print (zero terminated)
; ```
    pshs    x,a
@next
    lda     ,x+
    beq     @done
    bsr     PrintChar80x24
    bra     @next
@done
    puls    a,x,pc

;***********************
Clear80x24Line
;***********************
    pshs    y,x,a
    pshsw
    ;
    ; Map in $6C000 to our safeMmuLogicalBlock
    lda     #PHYSICAL_06C000_06DFFF
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    pshs    a
    ;
    ldy     safeMmuLogicalBlockAddress,pcr
    lda     printChar80x24_CurrentCursorY,pcr
@adjustForY
    tsta
    beq     @doneAdjustForY
    leay    160,y
    deca
    bra     @adjustForY
@doneAdjustForY
    clr     printChar80x24_CurrentCursorX,pcr
    ldx     #$2000  ; $20 = space, $00 is attrbute: no blink, no underline, index color 0 for background and foreground
    pshs    x
    ldw     #80
    tfm     s,y+
    puls    x
    ;
    ; Map the original physical block back into our safeMmuLogicalBlock
    puls    a
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    ;
    pulsw
    puls    a,x,y,pc


;***********************
PrintChar80x24
; ```plaintext
; > a = Character to print
; ```
    pshs    x,b,a
    pshsw
    tfr     a,e
    ; Map in $6C000 to our safeMmuLogicalBlock
    lda     #PHYSICAL_06C000_06DFFF
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    pshs    a
    ;
    ldx     safeMmuLogicalBlockAddress,pcr
    lda     printChar80x24_CurrentCursorY,pcr
@adjustForY
    tsta
    beq     @doneAdjustForY
    leax    160,x
    deca
    bra     @adjustForY
@doneAdjustForY
    ldb     printChar80x24_CurrentCursorX,pcr
    abx
    abx
    ; Print the character
    cmpe    #13                         ; Newline?
    bne     @not_newline
    ; Print a newline (blank rest of line, move cursor down one and all the way to the left)
    lda     printChar80x24_CurrentCursorX,pcr
    ldb     #32                         ; space
@blankRestOfLine
    tsta
    beq     @doneBlankingLine
    stb     ,x+
    clr     ,x+
    deca
    bra     @blankRestOfLine
@doneBlankingLine
    inc     printChar80x24_CurrentCursorY,pcr ; Move down one line
    clr     printChar80x24_CurrentCursorX,pcr ; Move back to beginning of line
    bra     @donePrint
    ; Print the character
@not_newline
    ste     ,x+
    clr     ,x+
    inc     printChar80x24_CurrentCursorX,pcr ; Move one character to the right
    ;
@donePrint
    lda     printChar80x24_CurrentCursorX,pcr
    cmpa    #80                               ; Did we just print in the right-most column?
    blt     @doneAdjustX
    inc     printChar80x24_CurrentCursorY,pcr ; Move down one line
    clr     printChar80x24_CurrentCursorX,pcr ; Move back to beginning of line
@doneAdjustX
    lda     printChar80x24_CurrentCursorY,pcr
    cmpa    #24                               ; Did we just move off the bottom of the screen?
    blt     @doneAdjustY
    clr     printChar80x24_CurrentCursorY,pcr ; Move back to the top of the screen
@doneAdjustY
    ; Map the original physical block back into our safeMmuLogicalBlock
    puls    a
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    ; Character printed.  We're out'a here!
    pulsw
    puls    a,b,x,pc

;***********************
UpdateRomCursorPosition:
;***********************
    pshs    b,a
    ldd     printChar80x24_CurrentCursorX   ;// Load cursor x into a and cursor y into b
    incb                                    ;// cursor y is 1 based for ROMS, 0 based for us
    std     $FE02                           ;// Store cursor x at $FE02 and cursor y at $FE03
    pshs    a                               ;// Save cusor x
    lda     #160
    mul                                     ;// d = 160 * cursor y
    addb    ,s
    adca    #0
    addb    ,s+                             ;// d += cursor x * 2
    adca    #0
    addd    #$2000                          ;// d += $2000
    std     $FE00                           ;// Store address of cursor at $FE00
    puls    a,b,pc

progressIndicator   fcn '.'
;***********************
PrintProgressIndicator:
;***********************
    pshs    x
    leax    progressIndicator,pcr
    lbsr    PrintString
    puls    x,pc
