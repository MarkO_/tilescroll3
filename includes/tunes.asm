;
tunes.land.index            equ 0
tunes.land.length           equ 1
tunes.land                  fcb 1,$2,$4F
;
tunes.hitBumper.index       equ 1
tunes.hitBumper.length      equ 1
tunes.hitBumper             fcb 1,$0B,$B2   ;  238 Hz * 256 / 5210 * 256 = $0BB2
;
tunes.jump.index            equ 2
tunes.jump.length           equ 4
tunes.jump                  fcb 1,$08,$5A  ;  170 Hz * 256 / 5210 * 256 = $085A
                            fcb 1,$09,$BB  ;  198 Hz * 256 / 5210 * 256 = $09BB
                            fcb 1,$08,$FE  ;  183 Hz * 256 / 5210 * 256 = $08FE
                            fcb 1,$0A,$9D  ;  216 Hz * 256 / 5210 * 256 = $0A9D
;
tunes.hitWingItem.index     equ 3
tunes.hitWingItem.length    equ 1
tunes.hitWingItem           fcb 174,$00,$E2  ;   18 Hz * 256 / 5210 * 256 = $00E2
;
tunes.hitGemItem.index      equ 4
tunes.hitGemItem.length     equ 3
tunes.hitGemItem            fcb 12,$0E,$72  ;  294 * 256 / 5210 * 256 = $0E72
                            fcb 12,$1C,$D8  ;  587 * 256 / 5210 * 256 = $1CD8
                            fcb 12,$39,$Bc  ; 1175 * 256 / 5210 * 256 = $39BC
;
tunes.jumpOpenDoor.index    equ 5
tunes.jumpOpenDoor.length   equ 2
tunes.jumpOpenDoor          fcb 2,$1D,$49  ;  596 Hz * 256 / 5210 * 256 = $1D49
                            fcb 2,$3A,$9E  ; 1193 Hz * 256 / 5210 * 256 = $3A9E
;
tunes.hitKeyItem.index     equ 6
tunes.hitKeyItem.length    equ 2
tunes.hitKeyItem           fcb 1,$1D,$49   ;  596 Hz * 256 / 5210 * 256 = $1D49
;
tunes.falling.index         equ 7
tunes.falling.length        equ 8
tunes.falling               fcb 2,$75,$3D  ; 2386 Hz * 256 / 5210 * 256 = $753D
                            fcb 2,$70,$B8  ; 2294 Hz * 256 / 5210 * 256 = $70B8
                            fcb 2,$6C,$8B  ; 2209 Hz * 256 / 5210 * 256 = $6C8B
                            fcb 2,$68,$A9  ; 2130 Hz * 256 / 5210 * 256 = $68A9
                            fcb 2,$65,$13  ; 2057 Hz * 256 / 5210 * 256 = $6513
                            fcb 2,$61,$AF  ; 1988 Hz * 256 / 5210 * 256 = $61AF
                            fcb 2,$5E,$8A  ; 1924 Hz * 256 / 5210 * 256 = $5E8A
                            fcb 4,$5B,$97  ; 1864 Hz * 256 / 5210 * 256 = $5B97

tunes.hitPaintItem.index    equ 8
tunes.hitPaintItem.length   equ 8
tunes.hitPaintItem          fcb 7,$02,$82  ;   51 Hz * 256 / 5210 * 256 = $0282
                            fcb 7,$02,$68  ;   49 Hz * 256 / 5210 * 256 = $0268
                            fcb 7,$02,$4F  ;   47 Hz * 256 / 5210 * 256 = $024F
                            fcb 7,$02,$36  ;   45 Hz * 256 / 5210 * 256 = $0236
                            fcb 7,$02,$29  ;   44 Hz * 256 / 5210 * 256 = $0229
                            fcb 7,$02,$10  ;   42 Hz * 256 / 5210 * 256 = $0210
                            fcb 7,$02,$04  ;   41 Hz * 256 / 5210 * 256 = $0204
                            fcb 7,$01,$EB  ;   39 Hz * 256 / 5210 * 256 = $01EB

tunes.hitHammerItem.index   equ 9
tunes.hitHammerItem.length  equ 2
tunes.hitHammerItem         fcb 3,$01,$C5  ;   36 Hz * 256 / 5210 * 256 = $01C5
                            fcb 2,$00,$00  ;    0 Hz * 256 / 5210 * 256 = $0000

tunes.bounceDamage.index    equ $0A
tunes.bounceDamage.length   equ 1
tunes.bounceDamage          fcb 3,$08,$FE  ;  183 Hz * 256 / 5210 * 256 = $08FE

tunes.hitDamageFloor.index  equ $0B
tunes.hitDamageFloor.length equ 3
tunes.hitDamageFloor        fcb 4,$01,$EB  ;   39 * 256 / 5210 * 256 = $01EB
                            fcb 2,$01,$DE  ;   38 * 256 / 5210 * 256 = $01DE
                            fcb 2,$01,$D1  ;   37 * 256 / 5210 * 256 = $01D1

tunes.hitFloorFall.index    equ $0C
tunes.hitFloorFall.length   equ 1
tunes.hitFloorFall          fcb 2,$EA,$7A  ; 4772 * 256 / 5210 * 256 = $EA7A

tunes.hitCrazyBummper.index     equ $0D
tunes.hitCrazyBummper.length    equ 3
tunes.hitCrazyBummper           fcb 1,$EA,$7A  ; 1403 * 256 / 5210 * 256 = $EA7A
                                fcb 1,$EA,$7A  ; 1194 * 256 / 5210 * 256 = $EA7A
                                fcb 1,$EA,$7A  ; 1355 * 256 / 5210 * 256 = $EA7A

tunes.death.index           equ $0E
tunes.death.length          equ 6
tunes.death                 fcb 3,$EA,$7A  ;   36 * 256 / 5210 * 256 = $EA7A
                            fcb 1,$00,$00  ;    0 * 256 / 5210 * 256 = $0000
                            fcb 2,$EA,$7A  ;   36 * 256 / 5210 * 256 = $EA7A
                            fcb 2,$00,$00  ;    0 * 256 / 5210 * 256 = $0000
                            fcb 1,$EA,$7A  ;   36 * 256 / 5210 * 256 = $EA7A
                            fcb 3,$00,$00  ;    0 * 256 / 5210 * 256 = $0000

tunes.ui.index              equ $0F
tunes.ui.length             equ 4
tunes.ui                    fcb 3,$EA,$7A  ;   47 * 256 / 5210 * 256 = $EA7A
                            fcb 5,$00,$00  ;    0 * 256 / 5210 * 256 = $0000
                            fcb 3,$EA,$7A  ;   47 * 256 / 5210 * 256 = $EA7A
                            fcb 5,$00,$00  ;    0 * 256 / 5210 * 256 = $0000

tunes.gameOver.index        equ $10
tunes.gameOver.length       equ 8
tunes.gameOver              fcb 2,$EA,$7A  ;  119 * 256 / 5210 * 256 = $EA7A
                            fcb 2,$00,$00  ;  108 * 256 / 5210 * 256 = $0000
                            fcb 2,$00,$00  ;   99 * 256 / 5210 * 256 = $0000
                            fcb 2,$00,$00  ;   91 * 256 / 5210 * 256 = $0000
                            fcb 2,$EA,$7A  ;   85 * 256 / 5210 * 256 = $EA7A
                            fcb 2,$00,$00  ;   79 * 256 / 5210 * 256 = $0000
                            fcb 2,$EA,$7A  ;   74 * 256 / 5210 * 256 = $EA7A
                            fcb 2,$00,$00  ;   70 * 256 / 5210 * 256 = $0000

tunes.hitClockItem.index    equ $11
tunes.hitClockItem.length   equ 4
tunes.hitClockItem          fcb 2,$EA,$7A  ;  879 * 256 / 5210 * 256 = $EA7A
                            fcb 2,$00,$00  ;  698 * 256 / 5210 * 256 = $0000
                            fcb 2,$00,$00  ;  783 * 256 / 5210 * 256 = $0000
                            fcb 3,$00,$00  ;  523 * 256 / 5210 * 256 = $0000

tunes.hitSodaItem.index     equ $12
tunes.hitSodaItem.length    equ 6
tunes.hitSodaItem           fcb 1,$EA,$7A  ; 2386 * 256 / 5210 * 256 = $EA7A
                            fcb 1,$00,$00  ; 2435 * 256 / 5210 * 256 = $0000
                            fcb 1,$00,$00  ; 2485 * 256 / 5210 * 256 = $0000
                            fcb 1,$00,$00  ; 2538 * 256 / 5210 * 256 = $0000
                            fcb 1,$EA,$7A  ; 2593 * 256 / 5210 * 256 = $EA7A
                            fcb 1,$00,$00  ; 2651 * 256 / 5210 * 256 = $0000

tunes.pause.index           equ $13
tunes.pause.length          equ 2
tunes.pause                 fcb 2,$EA,$7A  ; 1046 * 256 / 5210 * 256 = $EA7A
                            fcb 2,$00,$00  ;  879 * 256 / 5210 * 256 = $0000

tunes.ui2.index             equ $14
tunes.ui2.length            equ 1
tunes.ui2                   fcb 1,$EA,$7A  ; 1046 * 256 / 5210 * 256 = $EA7A

tunes.win.index             equ $15
tunes.win.length            equ 7
tunes.win                   fcb 2,$EA,$7A  ;  523 * 256 / 5210 * 256 = $EA7A
                            fcb 2,$00,$00  ;  659 * 256 / 5210 * 256 = $0000
                            fcb 2,$00,$00  ;  783 * 256 / 5210 * 256 = $0000
                            fcb 2,$00,$00  ; 1046 * 256 / 5210 * 256 = $0000
                            fcb 2,$00,$00  ;    0 * 256 / 5210 * 256 = $0000
                            fcb 2,$00,$00  ;  783 * 256 / 5210 * 256 = $0000
                            fcb 8,$EA,$7A  ; 1046 * 256 / 5210 * 256 = $EA7A

tunes.addresses             fdb tunes.land,       tunes.hitBumper,       tunes.jump,       tunes.hitWingItem,       tunes.hitGemItem,       tunes.jumpOpenDoor,       tunes.hitKeyItem,       tunes.falling,      tunes.hitPaintItem,        tunes.hitHammerItem,       tunes.bounceDamage,         tunes.hitDamageFloor,        tunes.hitFloorFall,        tunes.hitCrazyBummper,        tunes.death,        tunes.ui,        tunes.gameOver,        tunes.hitClockItem,        tunes.hitSodaItem,        tunes.pause,        tunes.ui2,        tunes.win.length
tunes.lengths               fcb tunes.land.length,tunes.hitBumper.length,tunes.jump.length,tunes.hitWingItem.length,tunes.hitGemItem.length,tunes.jumpOpenDoor.length,tunes.hitKeyItem.length,tunes.falling.index,tunes.hitPaintItem.length, tunes.hitHammerItem.length, tunes.bounceDamage.length, tunes.hitDamageFloor.length, tunes.hitFloorFall.length, tunes.hitCrazyBummper.length, tunes.death.length, tunes.ui.length, tunes.gameOver.length, tunes.hitClockItem.length, tunes.hitSodaItem.length, tunes.pause.length, tunes.ui2.length, tunes.win.length
