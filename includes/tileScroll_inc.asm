ConsoleInBufferFlag             equ $0070
ErrorHandlerVector              equ $0191
FileNameBuffer                  equ $094C
FileExtensionBuffer             equ $0954
ConsoleIn                       equ $A176
Width32                         equ $F652
Width80                         equ $F679
GetNextBasicCharacter           equ $9F
GetCurrentBasicCharacter        equ $A5

PIA0SideADataRegister_FF00      equ $FF00
PIA0SideAControlRegister_FF01   equ $FF01
PIA0SideBDataRegister_FF02      equ $FF02
PIA0SideBControlRegister_FF03   equ $FF03
PIA1SideADataRegister_FF20      equ $FF20
PIA1SideAControlRegister_FF21   equ $FF21
PIA1SideBDataRegister_FF22      equ $FF22
PIA1SideBControlRegister_FF23   equ $FF23
DiskController_FF40             equ $FF40
StatusCommand_FF48              equ $FF48
FDC_Track_FF49                  equ $FF49
FDC_Sector_FF4A                 equ $FF4A
FDC_Data_FF4B                   equ $FF4B

GIME_InitializeRegister0_FF90   equ $FF90
GIME_InitializeRegister1_FF91   equ $FF91
GIME_InterruptReqEnable_FF92    equ $FF92
GIME_FastInterruptReqEnable_FF93    equ $FF93
GIME_TimerMSB_FF94              equ $FF94
GIME_TimerLSB_FF95              equ $FF95
GIME_VideoMode_FF98             equ $FF98
GIME_VideoResolution_FF99       equ $FF99
GIME_BorderColor_FF9A           equ $FF9A
GIME_VideoBankSelect_FF9B       equ $FF9B
GIME_VerticalScroll_FF9C        equ $FF9C
GIME_VerticalOffset1_FF9D       equ $FF9D
GIME_VerticalOffset0_FF9E       equ $FF9E
GIME_HorizontalOffset_FF9F      equ $FF9F

MMU_BLOCK_REGISTERS_FIRST       equ $FFA0
ColorPaletteFirstRegister_FFB0  equ $FFB0
ClearClockSpeedR0_FFD6          equ $FFD6
SetClockSpeedR0_FFD7            equ $FFD7
ClearClockSpeedR1_FFD8          equ $FFD8
SetClockSpeedR1_FFD9            equ $FFD9
RomMode_FFDE                    equ $FFDE
RamMode_FFDF                    equ $FFDF

PHYSICAL_000000_001FFF          equ $00         ; Tile Data - 16 bytes (32 pixels wide) x 32 rows (1 tile) x 120 (total tiles) = 0xF000 (61440) bytes, 7.5 blocks
PHYSICAL_002000_003FFF          equ $01         ;                       |   ^^^ Tiles 0-15 ($00-$0F)    <<< Tiles 16-31 ($10-$1F)
PHYSICAL_004000_005FFF          equ $02         ;                       |   Tiles 32-47    ($20-$2F)
PHYSICAL_006000_007FFF          equ $03         ;                       |   Tiles 48-63    ($30-$3F)
PHYSICAL_008000_009FFF          equ $04         ;                       |   Tiles 64-79    ($40-$4F)
PHYSICAL_00A000_00BFFF          equ $05         ;                       |   Tiles 80-95    ($50-$5F)
PHYSICAL_00C000_00DFFF          equ $06         ;                       |   Tiles 96-111   ($60-$6F)
PHYSICAL_00E000_00FFFF          equ $07         ;              <--------/   Tiles 112-119  ($70-$77)
                                                ; ** Unused $00F000-$00FFFF, $1000 (4096) bytes
PHYSICAL_010000_011FFF          equ $08         ; Map Data - 32 bytes x 16 rows (1 map) x 32 maps = 2 blocks
PHYSICAL_012000_013FFF          equ $09         ;              <--------/
PHYSICAL_014000_015FFF          equ $0A         ; Hero Normal Routines (9 compiled sprites, idle and looking in 8 directions)
                                                ; ** Unused $0158B2-015FFF, $74E (1870) bytes
PHYSICAL_016000_017FFF          equ $0B         ; Hero Action Routines (17 compiled sprites, 3 closing eyes, 7 falling, 3 sparkles, 4 wings)
                                                ; ** Unused $017790-$017FFF, $870 (2160) bytes
PHYSICAL_018000_019FFF          equ $0C         ; Power Ups (7 compiled sprites)
                                                ; ** Unused $019353-$019FFF, $B17 (2839) bytes
PHYSICAL_01A000_01BFFF          equ $0D         ; 
PHYSICAL_01C000_01DFFF          equ $0E         ; 
PHYSICAL_01E000_01FFFF          equ $0F         ; 
PHYSICAL_020000_021FFF          equ $10         ; 
PHYSICAL_022000_023FFF          equ $11         ; 
PHYSICAL_024000_025FFF          equ $12         ; 
PHYSICAL_026000_027FFF          equ $13         ; 
PHYSICAL_028000_029FFF          equ $14         ; 
PHYSICAL_02A000_02BFFF          equ $15         ; 
PHYSICAL_02C000_02DFFF          equ $16         ; 
PHYSICAL_02E000_02FFFF          equ $17         ; 
PHYSICAL_030000_031FFF          equ $18         ; 
PHYSICAL_032000_033FFF          equ $19         ; 
PHYSICAL_034000_035FFF          equ $1A         ; 
PHYSICAL_036000_037FFF          equ $1B         ; 
PHYSICAL_038000_039FFF          equ $1C         ; 
PHYSICAL_03A000_03BFFF          equ $1D         ; 
PHYSICAL_03C000_03DFFF          equ $1E         ; 
PHYSICAL_03E000_03FFFF          equ $1F         ; 
PHYSICAL_040000_041FFF          equ $20         ; 
PHYSICAL_042000_043FFF          equ $21         ; 
PHYSICAL_044000_045FFF          equ $22         ; 
PHYSICAL_046000_047FFF          equ $23         ; 
PHYSICAL_048000_049FFF          equ $24         ; 
PHYSICAL_04A000_04BFFF          equ $25         ; 
PHYSICAL_04C000_04DFFF          equ $26         ; 
PHYSICAL_04E000_04FFFF          equ $27         ; 
PHYSICAL_050000_051FFF          equ $28         ; 
PHYSICAL_052000_053FFF          equ $29         ; 
PHYSICAL_054000_055FFF          equ $2A         ; 
PHYSICAL_056000_057FFF          equ $2B         ; 
PHYSICAL_058000_059FFF          equ $2C         ; 
PHYSICAL_05A000_05BFFF          equ $2D         ; 
PHYSICAL_05C000_05DFFF          equ $2E         ; 
PHYSICAL_05E000_05FFFF          equ $2F         ; 
PHYSICAL_060000_061FFF          equ $30         ; 
PHYSICAL_062000_063FFF          equ $31         ; 
PHYSICAL_064000_065FFF          equ $32         ; 
PHYSICAL_066000_067FFF          equ $33         ; 
PHYSICAL_068000_069FFF          equ $34         ; 
PHYSICAL_06A000_06BFFF          equ $35         ; 
PHYSICAL_06C000_06DFFF          equ $36         ; 
PHYSICAL_06E000_06FFFF          equ $37         ; 
PHYSICAL_070000_071FFF          equ $38         ; Original $0000-$1FFF Logical RAM - Swapped out for PHYSICAL_060000_061FFF
PHYSICAL_072000_073FFF          equ $39         ; Original $2000-$3FFF Logical RAM - Not swapped out
PHYSICAL_074000_075FFF          equ $3A         ; Original $4000-$5FFF Logical RAM - Not swapped out
PHYSICAL_076000_077FFF          equ $3B         ; Original $6000-$7FFF Logical RAM - Swapped out for PHYSICAL_066000_067FFF
PHYSICAL_078000_079FFF          equ $3C         ; Original $8000-$9FFF Logical RAM - Swapped out for PHYSICAL_068000_069FFF
PHYSICAL_07A000_07BFFF          equ $3D         ; Original $A000-$BFFF Logical RAM - Swapped out for PHYSICAL_06A000_06BFFF
PHYSICAL_07C000_07DFFF          equ $3E         ; Original $C000-$DFFF Logical RAM - Swapped out for PHYSICAL_06C000_06DFFF
PHYSICAL_07E000_07FFFF          equ $3F         ; Original $E000-$FFFF Logical RAM - Swapped out for PHYSICAL_06E000_06FFFF
;**************************************
PHYSICAL_080000_081FFF          equ $40         ; Buffer 1 - Prerendered Level 1st Slice - Pixels 0-255 - 256 bytes (512 pixels) x 512 rows - 16 Blocks
PHYSICAL_082000_083FFF          equ $41         ;                       |
PHYSICAL_084000_085FFF          equ $42         ;                       |
PHYSICAL_086000_087FFF          equ $43         ;                       |
PHYSICAL_088000_089FFF          equ $44         ;                       |
PHYSICAL_08A000_08BFFF          equ $45         ;                       |
PHYSICAL_08C000_08DFFF          equ $46         ;                       |
PHYSICAL_08E000_08FFFF          equ $47         ;                       |
PHYSICAL_090000_091FFF          equ $48         ;                       |
PHYSICAL_092000_093FFF          equ $49         ;                       |
PHYSICAL_094000_095FFF          equ $4A         ;                       |
PHYSICAL_096000_097FFF          equ $4B         ;                       |
PHYSICAL_098000_099FFF          equ $4C         ;                       |
PHYSICAL_09A000_09BFFF          equ $4D         ;                       |
PHYSICAL_09C000_09DFFF          equ $4E         ;                       |
PHYSICAL_09E000_09FFFF          equ $4F         ;              <--------/
PHYSICAL_0A0000_0A1FFF          equ $50         ; Buffer 1 - Prerendered Level 2nd Slice - Pixels 96-351 - 256 bytes (512 pixels) x 512 rows - 16 Blocks
PHYSICAL_0A2000_0A3FFF          equ $51         ;                       |
PHYSICAL_0A4000_0A5FFF          equ $52         ;                       |
PHYSICAL_0A6000_0A7FFF          equ $53         ;                       |
PHYSICAL_0A8000_0A9FFF          equ $54         ;                       |
PHYSICAL_0AA000_0ABFFF          equ $55         ;                       |
PHYSICAL_0AC000_0ADFFF          equ $56         ;                       |
PHYSICAL_0AE000_0AFFFF          equ $57         ;                       |
PHYSICAL_0B0000_0B1FFF          equ $58         ;                       |
PHYSICAL_0B2000_0B3FFF          equ $59         ;                       |
PHYSICAL_0B4000_0B5FFF          equ $5A         ;                       |
PHYSICAL_0B6000_0B7FFF          equ $5B         ;                       |
PHYSICAL_0B8000_0B9FFF          equ $5C         ;                       |
PHYSICAL_0BA000_0BBFFF          equ $5D         ;                       |
PHYSICAL_0BC000_0BDFFF          equ $5E         ;                       |
PHYSICAL_0BE000_0BFFFF          equ $5F         ;              <--------/
PHYSICAL_0C0000_0C1FFF          equ $60         ; Buffer 1 - Prerendered Level 3rd Slice - Pixels 192-447 - 256 bytes (512 pixels) x 512 rows - 16 Blocks
PHYSICAL_0C2000_0C3FFF          equ $61         ;                       |
PHYSICAL_0C4000_0C5FFF          equ $62         ;                       |
PHYSICAL_0C6000_0C7FFF          equ $63         ;                       |
PHYSICAL_0C8000_0C9FFF          equ $64         ;                       |
PHYSICAL_0CA000_0CBFFF          equ $65         ;                       |
PHYSICAL_0CC000_0CDFFF          equ $66         ;                       |
PHYSICAL_0CE000_0CFFFF          equ $67         ;                       |
PHYSICAL_0D0000_0D1FFF          equ $68         ;                       |
PHYSICAL_0D2000_0D3FFF          equ $69         ;                       |
PHYSICAL_0D4000_0D5FFF          equ $6A         ;                       |
PHYSICAL_0D6000_0D7FFF          equ $6B         ;                       |
PHYSICAL_0D8000_0D9FFF          equ $6C         ;                       |
PHYSICAL_0DA000_0DBFFF          equ $6D         ;                       |
PHYSICAL_0DC000_0DDFFF          equ $6E         ;                       |
PHYSICAL_0DE000_0DFFFF          equ $6F         ;              <--------/
PHYSICAL_0E0000_0E1FFF          equ $70         ; Buffer 1 - Prerendered Level 4th Slice - Pixels 256-511 - 256 bytes (512 pixels) x 512 rows - 16 Blocks
PHYSICAL_0E2000_0E3FFF          equ $71         ;                       |
PHYSICAL_0E4000_0E5FFF          equ $72         ;                       |
PHYSICAL_0E6000_0E7FFF          equ $73         ;                       |
PHYSICAL_0E8000_0E9FFF          equ $74         ;                       |
PHYSICAL_0EA000_0EBFFF          equ $75         ;                       |
PHYSICAL_0EC000_0EDFFF          equ $76         ;                       |
PHYSICAL_0EE000_0EFFFF          equ $77         ;                       |
PHYSICAL_0F0000_0F1FFF          equ $78         ;                       |
PHYSICAL_0F2000_0F3FFF          equ $79         ;                       |
PHYSICAL_0F4000_0F5FFF          equ $7A         ;                       |
PHYSICAL_0F6000_0F7FFF          equ $7B         ;                       |
PHYSICAL_0F8000_0F9FFF          equ $7C         ;                       |
PHYSICAL_0FA000_0FBFFF          equ $7D         ;                       |
PHYSICAL_0FC000_0FDFFF          equ $7E         ;                       |
PHYSICAL_0FE000_0FFFFF          equ $7F         ;              <--------/
;**************************************
PHYSICAL_100000_101FFF          equ $80         ; Buffer 2 - Prerendered Level 1st Slice - Pixels 0-255 - 256 bytes (512 pixels) x 512 rows - 16 Blocks
PHYSICAL_102000_103FFF          equ $81         ;                       |
PHYSICAL_104000_105FFF          equ $82         ;                       |
PHYSICAL_106000_107FFF          equ $83         ;                       |
PHYSICAL_108000_109FFF          equ $84         ;                       |
PHYSICAL_10A000_10BFFF          equ $85         ;                       |
PHYSICAL_10C000_10DFFF          equ $86         ;                       |
PHYSICAL_10E000_10FFFF          equ $87         ;                       |
PHYSICAL_110000_111FFF          equ $88         ;                       |
PHYSICAL_112000_113FFF          equ $89         ;                       |
PHYSICAL_114000_115FFF          equ $8A         ;                       |
PHYSICAL_116000_117FFF          equ $8B         ;                       |
PHYSICAL_118000_119FFF          equ $8C         ;                       |
PHYSICAL_11A000_11BFFF          equ $8D         ;                       |
PHYSICAL_11C000_11DFFF          equ $8E         ;                       |
PHYSICAL_11E000_11FFFF          equ $8F         ;              <--------/
PHYSICAL_120000_121FFF          equ $90         ; Buffer 2 - Prerendered Level 2nd Slice - Pixels 96-351 - 256 bytes (512 pixels) x 512 rows - 16 Blocks
PHYSICAL_122000_123FFF          equ $91         ;                       |
PHYSICAL_124000_125FFF          equ $92         ;                       |
PHYSICAL_126000_127FFF          equ $93         ;                       |
PHYSICAL_128000_129FFF          equ $94         ;                       |
PHYSICAL_12A000_12BFFF          equ $95         ;                       |
PHYSICAL_12C000_12DFFF          equ $96         ;                       |
PHYSICAL_12E000_12FFFF          equ $97         ;                       |
PHYSICAL_130000_131FFF          equ $98         ;                       |
PHYSICAL_132000_133FFF          equ $99         ;                       |
PHYSICAL_134000_135FFF          equ $9A         ;                       |
PHYSICAL_136000_137FFF          equ $9B         ;                       |
PHYSICAL_138000_139FFF          equ $9C         ;                       |
PHYSICAL_13A000_13BFFF          equ $9D         ;                       |
PHYSICAL_13C000_13DFFF          equ $9E         ;                       |
PHYSICAL_13E000_13FFFF          equ $9F         ;              <--------/
PHYSICAL_140000_141FFF          equ $A0         ; Buffer 2 - Prerendered Level 3rd Slice - Pixels 192-447 - 256 bytes (512 pixels) x 512 rows - 16 Blocks
PHYSICAL_142000_143FFF          equ $A1         ;                       |
PHYSICAL_144000_145FFF          equ $A2         ;                       |
PHYSICAL_146000_147FFF          equ $A3         ;                       |
PHYSICAL_148000_149FFF          equ $A4         ;                       |
PHYSICAL_14A000_14BFFF          equ $A5         ;                       |
PHYSICAL_14C000_14DFFF          equ $A6         ;                       |
PHYSICAL_14E000_14FFFF          equ $A7         ;                       |
PHYSICAL_150000_151FFF          equ $A8         ;                       |
PHYSICAL_152000_153FFF          equ $A9         ;                       |
PHYSICAL_154000_155FFF          equ $AA         ;                       |
PHYSICAL_156000_157FFF          equ $AB         ;                       |
PHYSICAL_158000_159FFF          equ $AC         ;                       |
PHYSICAL_15A000_15BFFF          equ $AD         ;                       |
PHYSICAL_15C000_15DFFF          equ $AE         ;                       |
PHYSICAL_15E000_15FFFF          equ $AF         ;              <--------/
PHYSICAL_160000_161FFF          equ $B0         ; Buffer 2 - Prerendered Level 4th Slice - Pixels 256-511 - 256 bytes (512 pixels) x 512 rows - 16 Blocks
PHYSICAL_162000_163FFF          equ $B1         ;                       |
PHYSICAL_164000_165FFF          equ $B2         ;                       |
PHYSICAL_166000_167FFF          equ $B3         ;                       |
PHYSICAL_168000_169FFF          equ $B4         ;                       |
PHYSICAL_16A000_16BFFF          equ $B5         ;                       |
PHYSICAL_16C000_16DFFF          equ $B6         ;                       |
PHYSICAL_16E000_16FFFF          equ $B7         ;                       |
PHYSICAL_170000_171FFF          equ $B8         ;                       |
PHYSICAL_172000_173FFF          equ $B9         ;                       |
PHYSICAL_174000_175FFF          equ $BA         ;                       |
PHYSICAL_176000_177FFF          equ $BB         ;                       |
PHYSICAL_178000_179FFF          equ $BC         ;                       |
PHYSICAL_17A000_17BFFF          equ $BD         ;                       |
PHYSICAL_17C000_17DFFF          equ $BE         ;                       |
PHYSICAL_17E000_17FFFF          equ $BF         ;              <--------/
;**************************************
PHYSICAL_180000_181FFF          equ $C0         ; 40x7 Text "score" area.  Drawn all over the 4th bank of 512k as the user scrolls around in the game.
PHYSICAL_182000_183FFF          equ $C1         ;                       |
PHYSICAL_184000_185FFF          equ $C2         ;                       |
PHYSICAL_186000_187FFF          equ $C3         ;                       |
PHYSICAL_188000_189FFF          equ $C4         ;                       |
PHYSICAL_18A000_18BFFF          equ $C5         ;                       |
PHYSICAL_18C000_18DFFF          equ $C6         ;                       |
PHYSICAL_18E000_18FFFF          equ $C7         ;                       |
PHYSICAL_190000_191FFF          equ $C8         ;                       |
PHYSICAL_192000_193FFF          equ $C9         ;                       |
PHYSICAL_194000_195FFF          equ $CA         ;                       |
PHYSICAL_196000_197FFF          equ $CB         ;                       |
PHYSICAL_198000_199FFF          equ $CC         ;                       |
PHYSICAL_19A000_19BFFF          equ $CD         ;                       |
PHYSICAL_19C000_19DFFF          equ $CE         ;                       |
PHYSICAL_19E000_19FFFF          equ $CF         ;                       |
PHYSICAL_1A0000_1A1FFF          equ $D0         ;                       |
PHYSICAL_1A2000_1A3FFF          equ $D1         ;                       |
PHYSICAL_1A4000_1A5FFF          equ $D2         ;                       |
PHYSICAL_1A6000_1A7FFF          equ $D3         ;                       |
PHYSICAL_1A8000_1A9FFF          equ $D4         ;                       |
PHYSICAL_1AA000_1ABFFF          equ $D5         ;                       |
PHYSICAL_1AC000_1ADFFF          equ $D6         ;                       |
PHYSICAL_1AE000_1AFFFF          equ $D7         ;                       |
PHYSICAL_1B0000_1B1FFF          equ $D8         ;                       |
PHYSICAL_1B2000_1B3FFF          equ $D9         ;                       |
PHYSICAL_1B4000_1B5FFF          equ $DA         ;                       |
PHYSICAL_1B6000_1B7FFF          equ $DB         ;                       |
PHYSICAL_1B8000_1B9FFF          equ $DC         ;                       |
PHYSICAL_1BA000_1BBFFF          equ $DD         ;                       |
PHYSICAL_1BC000_1BDFFF          equ $DE         ;                       |
PHYSICAL_1BE000_1BFFFF          equ $DF         ;                       |
PHYSICAL_1C0000_1C1FFF          equ $E0         ;                       |
PHYSICAL_1C2000_1C3FFF          equ $E1         ;                       |
PHYSICAL_1C4000_1C5FFF          equ $E2         ;                       |
PHYSICAL_1C6000_1C7FFF          equ $E3         ;                       |
PHYSICAL_1C8000_1C9FFF          equ $E4         ;                       |
PHYSICAL_1CA000_1CBFFF          equ $E5         ;                       |
PHYSICAL_1CC000_1CDFFF          equ $E6         ;                       |
PHYSICAL_1CE000_1CFFFF          equ $E7         ;                       |
PHYSICAL_1D0000_1D1FFF          equ $E8         ;                       |
PHYSICAL_1D2000_1D3FFF          equ $E9         ;                       |
PHYSICAL_1D4000_1D5FFF          equ $EA         ;                       |
PHYSICAL_1D6000_1D7FFF          equ $EB         ;                       |
PHYSICAL_1D8000_1D9FFF          equ $EC         ;                       |
PHYSICAL_1DA000_1DBFFF          equ $ED         ;                       |
PHYSICAL_1DC000_1DDFFF          equ $EE         ;                       |
PHYSICAL_1DE000_1DFFFF          equ $EF         ;                       |
PHYSICAL_1E0000_1E1FFF          equ $F0         ;                       |
PHYSICAL_1E2000_1E3FFF          equ $F1         ;                       |
PHYSICAL_1E4000_1E5FFF          equ $F2         ;                       |
PHYSICAL_1E6000_1E7FFF          equ $F3         ;                       |
PHYSICAL_1E8000_1E9FFF          equ $F4         ;                       |
PHYSICAL_1EA000_1EBFFF          equ $F5         ;                       |
PHYSICAL_1EC000_1EDFFF          equ $F6         ;                       |
PHYSICAL_1EE000_1EFFFF          equ $F7         ;                       |
PHYSICAL_1F0000_1F1FFF          equ $F8         ;                       |
PHYSICAL_1F2000_1F3FFF          equ $F9         ;                       |
PHYSICAL_1F4000_1F5FFF          equ $FA         ;                       |
PHYSICAL_1F6000_1F7FFF          equ $FB         ;                       |
PHYSICAL_1F8000_1F9FFF          equ $FC         ;                       |
PHYSICAL_1FA000_1FBFFF          equ $FD         ;                       |
PHYSICAL_1FC000_1FDFFF          equ $FE         ;                       |
PHYSICAL_1FE000_1FFFFF          equ $FF         ;              <--------/

LOGICAL_0000_1FFF               equ $00
LOGICAL_2000_3FFF               equ $01
LOGICAL_4000_5FFF               equ $02
LOGICAL_6000_7FFF               equ $03
LOGICAL_8000_9FFF               equ $04
LOGICAL_A000_BFFF               equ $05
LOGICAL_C000_DFFF               equ $06
LOGICAL_E000_FFFF               equ $07

DIRECTION_UP                    equ %0001
DIRECTION_DOWN                  equ %0010
DIRECTION_LEFT                  equ %0100
DIRECTION_RIGHT                 equ %1000
DIRECTION_UPLEFT                equ DIRECTION_UP|DIRECTION_LEFT
DIRECTION_UPRIGHT               equ DIRECTION_UP|DIRECTION_RIGHT
DIRECTION_DOWNLEFT              equ DIRECTION_DOWN|DIRECTION_LEFT
DIRECTION_DOWNRIGHT             equ DIRECTION_DOWN|DIRECTION_RIGHT

Sprites.Address.Loaded          equ $4000
Sprites.Address.Execute         equ $C000
Sprites.Address.Offset          equ Sprites.Address.Execute-Sprites.Address.Loaded
Hero.Idle                       equ Hero.00+Sprites.Address.Offset
Hero.UpLeft                     equ Hero.01+Sprites.Address.Offset
Hero.Up                         equ Hero.02+Sprites.Address.Offset
Hero.UpRight                    equ Hero.03+Sprites.Address.Offset
Hero.Right                      equ Hero.04+Sprites.Address.Offset
Hero.DownRight                  equ Hero.05+Sprites.Address.Offset
Hero.Down                       equ Hero.06+Sprites.Address.Offset
Hero.DownLeft                   equ Hero.07+Sprites.Address.Offset
Hero.Left                       equ Hero.08+Sprites.Address.Offset
Hero.Fall.Start                 equ Hero.09+Sprites.Address.Offset
Hero.Fall.Frames                equ 10
Hero.Sparkle.Start              equ Hero.19+Sprites.Address.Offset
Hero.Sparkle.Frames             equ 3
Hero.Wings.Start                equ Hero.22+Sprites.Address.Offset
Hero.Wings.Frames               equ 4
PowerUps.Key                    equ PowerUps.00+Sprites.Address.Offset
PowerUps.StopWatch              equ PowerUps.01+Sprites.Address.Offset
PowerUps.Soda                   equ PowerUps.02+Sprites.Address.Offset
PowerUps.Wings                  equ PowerUps.03+Sprites.Address.Offset
PowerUps.Shield                 equ PowerUps.04+Sprites.Address.Offset
PowerUps.Gem                    equ PowerUps.05+Sprites.Address.Offset
PowerUps.Paint                  equ PowerUps.06+Sprites.Address.Offset

TILE_DATA_PHYSICAL_BLOCK        equ PHYSICAL_000000_001FFF  ; 16 bytes (32 pixels) x 32 rows (1 tile) x 120 tiles = $F000 (61440) bytes = 7.5 blocks
MAP_DATA_PHYSICAL_BLOCK         equ PHYSICAL_010000_011FFF  ; 32 bytes x 16 rows (1 map) x 32 maps = $4000 (16383, 16K bytes = 2 blocks
HERO_NORMAL_DATA_PHYSICAL_BLOCK equ PHYSICAL_014000_015FFF  ; 9 compiled sprites, Hero.00 - Hero.08.  Hero idle, and looking in 8 directions
HERO_ACTION_DATA_PHYSICAL_BLOCK equ PHYSICAL_016000_017FFF  ; 17 compiled sprites, 3 closing eyes, 7 falling, 3 sparkles, 4 wings
POWERUPS_DATA_PHYSICAL_BLOCK    equ PHYSICAL_018000_019FFF  ; 7 compiled sprites, Key, Stop Watch, Soda, Wings, Shield, Gem, Paint
BUFFER1_VIDEO_BANK              equ 1
BUFFER1_FIRST_PHYSICAL_BLOCK    equ PHYSICAL_080000_081FFF  ; 256 bytes (512 pixels) x 512 rows x 4 slices = $80000 (524288, 512K) bytes = 64 blocks
BUFFER2_VIDEO_BANK              equ 2
BUFFER2_FIRST_PHYSICAL_BLOCK    equ PHYSICAL_100000_101FFF  ; 256 bytes (512 pixels) x 512 rows x 4 slices = $80000 (524288, 512K) bytes = 64 blocks