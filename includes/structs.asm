DebugData           STRUCT
gimeCheckCount                  rmb 2
highSpeedTimerCountStart        rmb 2
highSpeedTimerCountFinal        rmb 2
lastFrameVsyncCount             rmb 1
lastFrameFIRQCount              rmb 1
debugWord                       rmb 2
debugByte                       rmb 1
                    ENDSTRUCT

SpriteBackground    STRUCT
isCaptured                      rmb 1
firstOfTwoPhysicalBlocks        rmb 1
byteOffset                      rmb 2
buffer                          rmb 16*32
                    ENDSTRUCT

Sprite              STRUCT
isActive                        rmb 1
x                               rmb 2
y                               rmb 2
speedX                          rmb 1
speedY                          rmb 1
compiled                        rmb 2
compiledPhysicalBlock           rmb 1
background1                     SpriteBackground
background2                     SpriteBackground
onScreenBackgroundAddress       rmb 2
offScreenBackgroundAddress      rmb 2
                    ENDSTRUCT

Tone                STRUCT
duration                        rmb 1
frequencyValue                  rmb 2
                    ENDSTRUCT