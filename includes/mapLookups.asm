palettes                       ;       0       1       2       3       4       5       6       7       8       9       10      11      12      13      14      15
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 0
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%011000,%111000,%000111,%001011,%010111,%011011,%001011,%011011,%110110,%111111 ; Level 1
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 2
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 3
                                fcb %000000,%001000,%010000,%011000,%010000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%010111,%011011,%110110,%111111 ; Level 4
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 5
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%011000,%111000,%000111,%001011,%010111,%011011,%001011,%011011,%110110,%111111 ; Level 6
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 7
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%101000,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 8
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 9
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%011000,%111000,%000111,%001011,%010111,%011011,%001011,%011011,%110110,%111111 ; Level 10
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 11
                                fcb %000000,%001000,%010000,%011000,%100000,%110110,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%100100,%110110,%111111 ; Level 12
                                fcb %000000,%001000,%010000,%011000,%010000,%011000,%010000,%111000,%000111,%001011,%010111,%011011,%010111,%011011,%110110,%111111 ; Level 13
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%101000,%111000,%000111,%001011,%010111,%011011,%101101,%011011,%110110,%111111 ; Level 14
                                fcb %000000,%001000,%010000,%011000,%100000,%010000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%010111,%110110,%111111 ; Level 15
                                fcb %000000,%001000,%010000,%011000,%000111,%011000,%000111,%111000,%000111,%001011,%010111,%011011,%111000,%011011,%110110,%111111 ; Level 16
                                fcb %000000,%001000,%010000,%001000,%010000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%010111,%011011,%110110,%111111 ; Level 17
                                fcb %000000,%001000,%010000,%011000,%000111,%011000,%000111,%111000,%000111,%001011,%010111,%011011,%111000,%011011,%110110,%111111 ; Level 18
                                fcb %000000,%001000,%010000,%011000,%000111,%011000,%000111,%111000,%000111,%001011,%010111,%011011,%111000,%011011,%110110,%111111 ; Level 19
                                fcb %000000,%001000,%010000,%011000,%010000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%010111,%011011,%110110,%111111 ; Level 20
                                fcb %000000,%001000,%010000,%001000,%001000,%011000,%001011,%111000,%000111,%001011,%010111,%011011,%011000,%011011,%110110,%111111 ; Level 21
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%101000,%111000,%000111,%001011,%010111,%011011,%101101,%011011,%110110,%111111 ; Level 22
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 23
                                fcb %000000,%001000,%010000,%011000,%000111,%011000,%000111,%111000,%000111,%001011,%010111,%011011,%111000,%011011,%110110,%111111 ; Level 24
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 25
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 26
                                fcb %000000,%001000,%010000,%100000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%100100,%110110,%111111 ; Level 27
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%101000,%111000,%000111,%001011,%010111,%011011,%101101,%011011,%110110,%111111 ; Level 28
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%011000,%111000,%000111,%001011,%010111,%011011,%001011,%011011,%110110,%111111 ; Level 29
                                fcb %000000,%001000,%010000,%011000,%000111,%011000,%000111,%111000,%000111,%001011,%010111,%011011,%111000,%011011,%110110,%111111 ; Level 30
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%101000,%111000,%000111,%001011,%010111,%011011,%101101,%011011,%110110,%111111 ; Level 31

mapToTileLookup                 fcb     $25     ; 00 ($00) = No Floor, Fall Through, Stars shine through
                                fcb     $6B     ; 01 ($01) = Floor Current, no item
                                fcb     $28     ; 02 ($02) = Blue 4-spike wall
                                fcb     $2E     ; 03 ($03) = Floor Red
                                fcb     $2D     ; 04 ($04) = Floor Purple
                                fcb     $06     ; 05 ($05) = Brown Pipes Wall Horizontal, Floor Top, Connector Bottom
                                fcb     $14     ; 06 ($06) = Brown Pipes Wall Vertical, Floor Left, Connector Right
                                fcb     $1A     ; 07 ($07) = Brown Pipes Wall Top Left Corner, Floor Outer
                                fcb     $1C     ; 08 ($08) = Brown Pipes Wall Top Right Corner, Floor Outer
                                fcb     $1B     ; 09 ($09) = Brown Pipes Wall Bottom Left Corner, Floor Outer
                                fcb     $08     ; 10 ($0A) = Brown Pipes Wall Bottom Right Corner, Floor Outer
                                fcb     $18     ; 11 ($0B) = Brown Pipes Wall Down T
                                fcb     $15     ; 12 ($0C) = Brown Pipes Wall Up T
                                fcb     $19     ; 13 ($0D) = Brown Pipes Wall Right T
                                fcb     $17     ; 14 ($0E) = Brown Pipes Wall Left T
                                fcb     $16     ; 15 ($0F) = Brown Pipes Wall Plus
                                fcb     $04     ; 16 ($10) = Brown Pipes Wall Horizontal, Black Top, Connector Bottom
                                fcb     $26     ; 17 ($11) = Brown Pipes Wall Horizontal, Floor Top, Black Bottom
                                fcb     $01     ; 18 ($12) = Brown Pipes Wall Vertical, Black Left, Connector Right
                                fcb     $1E     ; 19 ($13) = Brown Pipes Wall Vertical, Floor Left, Black Right
                                fcb     $00     ; 20 ($14) = Brown Pipes Wall Top Left Corner, Black Outer
                                fcb     $09     ; 21 ($15) = Brown Pipes Wall Top Right Corner, Black Outer
                                fcb     $02     ; 22 ($16) = Brown Pipes Wall Bottom Left Corner, Black Outer
                                fcb     $1D     ; 23 ($17) = Brown Pipes Wall Bottom Right Corner, Black Outer
                                fcb     $20     ; 24 ($18) = Bumper Horizontal
                                fcb     $27     ; 25 ($19) = Bumper Vertical
                                fcb     $1F     ; 26 ($1A) = Bumper Top Left Corner
                                fcb     $21     ; 27 ($1B) = Bumper Top Right Corner
                                fcb     $2B     ; 28 ($1C) = Bumper Bottom Left Corner
                                fcb     $2C     ; 29 ($1D) = Bumper Bottom Right Corner
                                fcb     $37     ; 30 ($1E) = Bumper Down T
                                fcb     $38     ; 31 ($1F) = Bumper Up T
                                fcb     $36     ; 32 ($20) = Bumper Right T
                                fcb     $39     ; 33 ($21) = Bumper Left T
                                fcb     $2A     ; 34 ($22) = Bumper Dot
                                fcb     $22     ; 35 ($23) = Bumper Vertical Top End Cap
                                fcb     $29     ; 36 ($24) = Bumper Vertical Bottom End Cap
                                fcb     $32     ; 37 ($25) = Bumper Horizontal Left End Cap
                                fcb     $33     ; 38 ($26) = Bumper Horizontal Right End Cap
                                fcb     $0C     ; 39 ($27) = Push Left
                                fcb     $0D     ; 40 ($28) = Push Right
                                fcb     $0E     ; 41 ($29) = Push Up
                                fcb     $0B     ; 42 ($2A) = Push Down
                                fcb     $10     ; 43 ($2B) = Push Up Left
                                fcb     $0F     ; 44 ($2C) = Push Up Right
                                fcb     $11     ; 45 ($2D) = Push Down Left
                                fcb     $12     ; 46 ($2E) = Push Down Right
                                fcb     $0A     ; 47 ($2F) = Door Vertical Pipes
                                fcb     $07     ; 48 ($30) = Door Horizontal Pipes
                                fcb     $03     ; 49 ($31) = Floor Ice
                                fcb     $6B     ; 50 ($32) = Floor Current, No Item, Fall Through
                                fcb     $25     ; 51 ($33) = Floor Black?
                                fcb     $2F     ; 52 ($34) = Floor Green
                                fcb     $13     ; 53 ($35) = Bumper Crazy
                                fcb     $24     ; 54 ($36) = Magnet
                                fcb     $6B     ; 55 ($37) = Floor Current, No Item, Wall For Enemy, Not Wall For Player
                                fcb     $23     ; 56 ($38) = Floor Pink
                                fcb     $6B     ; 57 ($39) = Floor Current, No Item, Only Used In Unseen Kingdom (no idea why 1 wasn't used)
                                fcb     $25     ; 58 ($3A) = Floor Black?
                                fcb     $6B     ; 59 ($3B) = Floor Current, No Item, Wall Horizontal
                                fcb     $6B     ; 60 ($3C) = Floor Current, No Item, Wall Vertical
                                fcb     $6B     ; 61 ($3D) = Floor Current, No Item, Wall Top Left Corner
                                fcb     $6B     ; 62 ($3E) = Floor Current, No Item, Wall Top Right Corner
                                fcb     $6B     ; 63 ($3F) = Floor Current, No Item, Wall Bottom Left Corner
                                fcb     $6B     ; 64 ($40) = Floor Current, No Item, Wall Bottom Right Corner
                                fcb     $6B     ; 65 ($41) = Floor Current, No Item, Wall Down T
                                fcb     $6B     ; 66 ($42) = Floor Current, No Item, Wall Up T
                                fcb     $6B     ; 67 ($43) = Floor Current, No Item, Wall Right T
                                fcb     $6B     ; 68 ($44) = Floor Current, No Item, Wall Left T
                                fcb     $6B     ; 69 ($45) = Floor Current, No Item, Wall Plus
                                fcb     $69     ; 70 ($46) = Floor Current, Gem Item
                                fcb     $6A     ; 71 ($47) = Floor Current, Paint Item
                                fcb     $67     ; 72 ($48) = Floor Current, Wing Item
                                fcb     $64     ; 73 ($49) = Floor Current, Key Item
                                fcb     $65     ; 74 ($4A) = Floor Current, Clock Item
                                fcb     $66     ; 75 ($4B) = Floor Current, Soda Item
                                fcb     $68     ; 76 ($4C) = Floor Current, Hammer Item
                                fcb     $41     ; 77 ($4D) = Brown Stone Wall
                                fcb     $48     ; 78 ($4E) = Green Skull
                                fcb     $4B     ; 79 ($4F) = Yellow Pillar
                                fcb     $4C     ; 80 ($50) = Floor (?) Brown 4 Spheres
                                fcb     $4E     ; 81 ($51) = Brown Ball, Black Outer
                                fcb     $42     ; 82 ($52) = Door Vertical Stone
                                fcb     $43     ; 83 ($53) = Door Horizontal Stone
                                fcb     $4A     ; 84 ($54) = Door Vertical Green
                                fcb     $49     ; 85 ($55) = Door Horizontal Green
                                fcb     $6E     ; 86 ($56) = Cloud 3
                                fcb     $6F     ; 87 ($57) = Cloud 4
                                fcb     $70     ; 88 ($58) = Cloud 5
                                fcb     $71     ; 89 ($59) = Cloud 6
                                fcb     $72     ; 90 ($5A) = Cloud 7
                                fcb     $73     ; 91 ($5B) = Cloud 8
                                fcb     $74     ; 92 ($5C) = Cloud 9
                                fcb     $75     ; 93 ($5D) = Cloud 10
                                fcb     $6C     ; 94 ($5E) = Cloud 1
                                fcb     $6D     ; 95 ($5F) = Cloud 2
                                fcb     $76     ; 96 ($60) = Cloud 11
                                fcb     $77     ; 97 ($61) = Cloud 12

levelToFloorTileLookup          fcb     $05     ; 0
                                fcb     $28     ; 1
                                fcb     $05     ; 2
                                fcb     $4D     ; 3
                                fcb     $4F     ; 4
                                fcb     $3F     ; 5
                                fcb     $3C     ; 6
                                fcb     $40     ; 7
                                fcb     $3D     ; 8
                                fcb     $40     ; 9
                                fcb     $47     ; 10
                                fcb     $4D     ; 11
                                fcb     $3F     ; 12
                                fcb     $4B     ; 13
                                fcb     $4C     ; 14
                                fcb     $05     ; 15
                                fcb     $3D     ; 16
                                fcb     $45     ; 17
                                fcb     $3F     ; 18
                                fcb     $40     ; 19
                                fcb     $4C     ; 20
                                fcb     $4B     ; 21
                                fcb     $4F     ; 22
                                fcb     $05     ; 23
                                fcb     $40     ; 24
                                fcb     $45     ; 25
                                fcb     $3C     ; 26
                                fcb     $3F     ; 27
                                fcb     $05     ; 28
                                fcb     $28     ; 29
                                fcb     $45     ; 30
                                fcb     $3D     ; 31

mapDataToHeroActionIndexLookup  fcb     $0B     ; 00 = HeroHitNoFloorFall
                                fcb     $01     ; 01 = HeroHitNormalFloor
                                fcb     $01     ; 02 = HeroHitNormalFloor
                                fcb     $0F     ; 03 = HeroHitDamagingFloor
                                fcb     $0F     ; 04 = HeroHitDamagingFloor
                                fcb     $00     ; 05 = HeroHitWall
                                fcb     $00     ; 06 = HeroHitWall
                                fcb     $00     ; 07 = HeroHitWall
                                fcb     $00     ; 08 = HeroHitWall
                                fcb     $00     ; 09 = HeroHitWall
                                fcb     $00     ; 10 = HeroHitWall
                                fcb     $00     ; 11 = HeroHitWall
                                fcb     $00     ; 12 = HeroHitWall
                                fcb     $00     ; 13 = HeroHitWall
                                fcb     $00     ; 14 = HeroHitWall
                                fcb     $00     ; 15 = HeroHitWall
                                fcb     $00     ; 16 = HeroHitWall
                                fcb     $00     ; 17 = HeroHitWall
                                fcb     $00     ; 18 = HeroHitWall
                                fcb     $00     ; 19 = HeroHitWall
                                fcb     $00     ; 20 = HeroHitWall
                                fcb     $00     ; 21 = HeroHitWall
                                fcb     $00     ; 22 = HeroHitWall
                                fcb     $00     ; 23 = HeroHitWall
                                fcb     $0D     ; 24 = HeroHitBumper
                                fcb     $0D     ; 25 = HeroHitBumper
                                fcb     $0D     ; 26 = HeroHitBumper
                                fcb     $0D     ; 27 = HeroHitBumper
                                fcb     $0D     ; 28 = HeroHitBumper
                                fcb     $0D     ; 29 = HeroHitBumper
                                fcb     $0D     ; 30 = HeroHitBumper
                                fcb     $0D     ; 31 = HeroHitBumper
                                fcb     $0D     ; 32 = HeroHitBumper
                                fcb     $0D     ; 33 = HeroHitBumper
                                fcb     $0D     ; 34 = HeroHitBumper
                                fcb     $0D     ; 35 = HeroHitBumper
                                fcb     $0D     ; 36 = HeroHitBumper
                                fcb     $0D     ; 37 = HeroHitBumper
                                fcb     $0D     ; 38 = HeroHitBumper
                                fcb     $02     ; 39 = HeroHitPushLeft
                                fcb     $03     ; 40 = HeroHitPushRight
                                fcb     $04     ; 41 = HeroHitPushUp
                                fcb     $05     ; 42 = HeroHitPushDown
                                fcb     $06     ; 43 = HeroHitPushLeftUp
                                fcb     $07     ; 44 = HeroHitPushRightUp
                                fcb     $08     ; 45 = HeroHitPushLeftDown
                                fcb     $09     ; 46 = HeroHitPushRightDown
                                fcb     $0C     ; 47 = HeroHitWall
                                fcb     $0C     ; 48 = HeroHitWall
                                fcb     $0A     ; 49 = HeroHitIceFloor
                                fcb     $0E     ; 50 = HeroHitFloorFall
                                fcb     $00     ; 51 = HeroHitWall
                                fcb     $0F     ; 52 = HeroHitDamagingFloor
                                fcb     $10     ; 53 = HeroHitCrazyBumper
                                fcb     $11     ; 54 = Magenet
                                fcb     $01     ; 55 = HeroHitNormalFloor
                                fcb     $0F     ; 56 = HeroHitDamagingFloor
                                fcb     $01     ; 57 = HeroHitNormalFloor
                                fcb     $0B     ; 58 = HeroHitNoFloorFall
                                fcb     $00     ; 59 = HeroHitWall
                                fcb     $00     ; 60 = HeroHitWall
                                fcb     $00     ; 61 = HeroHitWall
                                fcb     $00     ; 62 = HeroHitWall
                                fcb     $00     ; 63 = HeroHitWall
                                fcb     $00     ; 64 = HeroHitWall
                                fcb     $00     ; 65 = HeroHitWall
                                fcb     $00     ; 66 = HeroHitWall
                                fcb     $00     ; 67 = HeroHitWall
                                fcb     $00     ; 68 = HeroHitWall
                                fcb     $00     ; 69 = HeroHitWall
                                fcb     $12     ; 70 = HeroHitGem
                                fcb     $13     ; 71 = HeroHitPaint
                                fcb     $14     ; 72 = HeroHitWing
                                fcb     $15     ; 73 = HeroHitKey
                                fcb     $16     ; 74 = HeroHitClock
                                fcb     $17     ; 75 = HeroHitSoda
                                fcb     $18     ; 76 = HeroHitHammer
                                fcb     $00     ; 77 = HeroHitWall
                                fcb     $00     ; 78 = HeroHitWall
                                fcb     $00     ; 79 = HeroHitWall
                                fcb     $00     ; 80 = HeroHitWall
                                fcb     $00     ; 81 = HeroHitWall
                                fcb     $0C     ; 82 = HeroHitWall
                                fcb     $0C     ; 83 = HeroHitWall
                                fcb     $0C     ; 84 = HeroHitWall
                                fcb     $0C     ; 85 = HeroHitWall
                                fcb     $00     ; 86 = HeroHitWall
                                fcb     $00     ; 87 = HeroHitWall
                                fcb     $00     ; 88 = HeroHitWall
                                fcb     $00     ; 89 = HeroHitWall
                                fcb     $00     ; 90 = HeroHitWall
                                fcb     $00     ; 91 = HeroHitWall
                                fcb     $00     ; 92 = HeroHitWall
                                fcb     $00     ; 93 = HeroHitWall
                                fcb     $00     ; 94 = HeroHitWall
                                fcb     $00     ; 95 = HeroHitWall
                                fcb     $00     ; 96 = HeroHitWall
                                fcb     $00     ; 97 = HeroHitWall

heroActionLookup                fdb      actionHeroHitNoAction      ; 0x00 = Walls
                                fdb      actionHeroHitNoAction      ; 0x01 = Normal Floors
                                fdb      actionHeroHitPushLeft      ; 0x02
                                fdb      actionHeroHitPushRight     ; 0x03
                                fdb      actionHeroHitPushUp        ; 0x04
                                fdb      actionHeroHitPushDown      ; 0x05
                                fdb      actionHeroHitPushLeftUp    ; 0x06
                                fdb      actionHeroHitPushRightUp   ; 0x07
                                fdb      actionHeroHitPushLeftDown  ; 0x08
                                fdb      actionHeroHitPushRightDown ; 0x09
                                fdb      actionHeroHitIceFloor      ; 0x0A
                                fdb      actionHeroHitNoFloorFall   ; 0x0B
                                fdb      actionHeroHitNoAction      ; 0x0C = Doors
                                fdb      actionHeroHitNoAction      ; 0x0D = Bumpers
                                fdb      actionHeroHitFloorFall     ; 0x0E
                                fdb      actionHeroHitDamagingFloor ; 0x0F
                                fdb      actionHeroHitCrazyBumper   ; 0x10
                                fdb      actionHeroHitMagnet        ; 0x11
                                fdb      actionHeroHitGemItem       ; 0x12
                                fdb      actionHeroHitPaintItem     ; 0x13
                                fdb      actionHeroHitWingItem      ; 0x14
                                fdb      actionHeroHitKeyItem       ; 0x15
                                fdb      actionHeroHitClockItem     ; 0x16
                                fdb      actionHeroHitSodaItem      ; 0x17
                                fdb      actionHeroHitHammerItem    ; 0x18
